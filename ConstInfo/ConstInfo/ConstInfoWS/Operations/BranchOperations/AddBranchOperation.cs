﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.BranchOperations
{
    public class AddBranchOperation : BaseOperation<AddBranchOperation, GetResult<Branch>>
    {
        Branch _branch;

        public AddBranchOperation(Branch branch)
        {
            _branch = branch;
        }

        public override GetResult<Branch> BeforeOp()
        {
            CompositeValidator validator = new CompositeValidator();
            validator.Add(new CheckValidObject(_branch));
            BaseResult result = validator.Validate();
            return new GetResult<Branch> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }
        public override GetResult<Branch> InOperation()
        {
            _branch = db.InsertBranch(_branch);
            if (_branch.Id != 0)
            {
                return new GetResult<Branch>() { Result = _branch };
            }
            else return new GetResult<Branch> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };

        }
    }
}