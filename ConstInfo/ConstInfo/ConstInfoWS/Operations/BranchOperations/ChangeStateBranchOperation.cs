﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.BranchOperations
{
    public class ChangeStateBranchOperation : BaseOperation<ChangeStateBranchOperation, BaseResult>
    {
        int _id;
        bool _delete;
        public ChangeStateBranchOperation(int id, bool delete)
        {
            _id = id;
            _delete = delete;
        }

        public override BaseResult BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckIdBranch(_id, db));
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return result;

        }
        public override BaseResult InOperation()
        {
            db.ChangeStateBranch(_id, _delete);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };

        }
    }
}