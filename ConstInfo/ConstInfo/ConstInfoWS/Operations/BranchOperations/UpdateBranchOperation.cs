﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.BranchOperations
{
    public class UpdateBranchOperation : BaseOperation<UpdateBranchOperation, BaseResult>
    {
        Branch _branch;

        public UpdateBranchOperation(Branch branch)
        {
            _branch = branch;
        }

        public override BaseResult BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckIdBranch(_branch.Id, db));
            validator.Add(new CheckValidObject(_branch));
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return result;

        }

        public override BaseResult InOperation()
        {
            db.UpdateBranch(_branch);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
        }
    }
}