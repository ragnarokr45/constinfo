﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.BranchOperations
{
    public class GetOneBranchOperation : BaseOperation<GetOneBranchOperation, GetResult<Branch>>
    {
        int _id;
        Branch _branch;

        public GetOneBranchOperation(int id)
        {
            _id = id;
        }

        public override GetResult<Branch> BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckIdBranch(_id, db));
            BaseResult result = validator.Validate();
            return new GetResult<Branch> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }

        public override GetResult<Branch> InOperation()
        {
            _branch = db.GetBranch(_id);
            if (_branch.Id != 0)
            {
                return new GetResult<Branch>() { Result = _branch };
            }
            else return new GetResult<Branch> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };

        }
    }
}