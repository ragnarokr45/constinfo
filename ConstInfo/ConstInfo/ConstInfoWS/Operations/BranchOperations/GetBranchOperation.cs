﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.BranchOperations
{
    public class GetBranchOperation : BaseOperation<GetBranchOperation, GetResult<List<Branch>>>
    {
        bool _showDeleted;


        public GetBranchOperation(bool showDeleted)
        {
            _showDeleted = showDeleted;

        }


        public override GetResult<List<Branch>> InOperation()
        {
            return new GetResult<List<Branch>>() { Result = db.GetBranchs(_showDeleted) };
        }
    }
}