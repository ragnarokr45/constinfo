﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using NLog;
using ConstInfoWS.Serializers;

namespace ConstInfoWS.Operations
{
    public abstract class BaseOperation<O, T> where O : BaseOperation<O, T> where T : BaseResult, new()
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected DBConnection db;
        public virtual T BeforeOp()
        {
            return null;
        }

        public abstract T InOperation();

        public virtual void AfterOp()
        {
            db.Dispose();
            logger.Debug($"Database connection disconnected");
        }

        private T ExecuteOperation()
        {
            db = new DBConnection();
            logger.Debug($"Database connection established");
            var operationResult = BeforeOp();
            T result = null;
            try
            {
                if (operationResult != null && operationResult.ErrorCode != ErrorCode.Success)
                {
                    logger.Info($"{this.GetType()}: finished with result: {operationResult.ErrorCode}");
                    return operationResult;
                }

                result = InOperation();
            }
            catch (Exception ex)
            {
                result = new T();
                result.ErrorCode = ErrorCode.CustomError;
                result.ErrorDescription = ex.Message;
                logger.Error(ex, $"{this.GetType()}: throw an exception: {ex}");
                return result;
            }       
            finally
            {
                AfterOp();
            }
            return result;
        }

        public static T Execute(params object[] values)
        {
            var operation = (O)Activator.CreateInstance(typeof(O), values);
            logger.Debug($"{operation.GetType()} was called");
            var result = operation.ExecuteOperation();

            string dataStr = "none" ;
            if (result != null)
            {
               dataStr = result.Serialize<T>();
            }

            logger.Debug($"{operation.GetType()}: finished with result: {result.ErrorCode}, content: {dataStr}");

            return result;
            
        }

    }
}