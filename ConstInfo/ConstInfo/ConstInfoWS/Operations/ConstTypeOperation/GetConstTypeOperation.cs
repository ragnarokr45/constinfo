﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.ConstTypeOperation
{
    public class GetConstTypeOperation : BaseOperation<GetConstTypeOperation, GetResult<List<ConstType>>>
    {
        bool _showDeleted;
        

        public GetConstTypeOperation(bool showDeleted)
        {
            _showDeleted = showDeleted;
           
        }

 
        public override GetResult<List<ConstType>> InOperation()
        {
            return new GetResult<List<ConstType>>() { Result = db.GetConstTypes(_showDeleted) };
        }
    }
}