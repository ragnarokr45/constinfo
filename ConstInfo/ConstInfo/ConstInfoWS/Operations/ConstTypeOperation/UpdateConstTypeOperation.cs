﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;
using ConstInfoWS.Models;

namespace ConstInfoWS.Operations.ConstTypeOperation
{
    public class UpdateConstTypeOperation : BaseOperation<UpdateConstTypeOperation, BaseResult>
    {
        ConstType _ct;
        User _user;
        public UpdateConstTypeOperation(User user, ConstType ct)
        {
            _ct = ct;
            _user = user;
        }

        public override BaseResult BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckIdConstType(_ct.Id, db));
            validator.Add(new CheckValidObject(_ct));
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return result;

        }

        public override BaseResult InOperation()
        {
            db.UpdateConstType(_ct);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
        }
    }
}