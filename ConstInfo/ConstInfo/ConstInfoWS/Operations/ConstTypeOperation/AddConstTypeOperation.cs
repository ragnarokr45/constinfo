﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;
using ConstInfoWS.Models;

namespace ConstInfoWS.Operations.ConstTypeOperation
{
    public class AddConstTypeOperation : BaseOperation<AddConstTypeOperation, GetResult<ConstType>>
    {
        ConstType _ct;
        User _user;
        public AddConstTypeOperation(User user, ConstType ct)
        {
            _ct = ct;
            _user = user;
        }

        public override GetResult<ConstType> BeforeOp()
        {
            CompositeValidator validator = new CompositeValidator();
            validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckValidObject(_ct));
            BaseResult result = validator.Validate();
            return new GetResult<ConstType> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }
        public override GetResult<ConstType> InOperation()
        {
            _ct = db.InsertConstType(_ct);
             if (_ct.Id != 0)
            {
                return new GetResult<ConstType>() { Result = _ct };
            }
            else return new GetResult<ConstType> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };

        }
    }
}