﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.ConstTypeOperation
{
    public class GetOneConstTypeOperation : BaseOperation<GetOneConstTypeOperation, GetResult<ConstType>>
    {
        int _id;
        ConstType _ct;
        public GetOneConstTypeOperation(int id)
        {
            _id = id;
        }

        public override GetResult<ConstType> BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckIdConstType(_id, db));
            BaseResult result = validator.Validate();
            return new GetResult<ConstType> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }

        public override GetResult<ConstType> InOperation()
        {
            _ct = db.GetConstType(_id);
            if (_ct.Id != 0)
            {
                return new GetResult<ConstType>() { Result = _ct };
            }
            else return new GetResult<ConstType> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };

        }
    }
}