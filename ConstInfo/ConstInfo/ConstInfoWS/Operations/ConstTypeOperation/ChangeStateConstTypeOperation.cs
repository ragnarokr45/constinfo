﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.ConstTypeOperation
{
    public class ChangeStateConstTypeOperation : BaseOperation<ChangeStateConstTypeOperation, BaseResult>
    {
        int _id;
        User _user;
        bool _delete;
        public ChangeStateConstTypeOperation(int id, User user, bool delete)
        {
            _id = id;
            _user = user;
            _delete = delete;
        }

        public override BaseResult BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckIdConstType(_id, db));
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return result;

        }
        public override BaseResult InOperation()
        {
            db.ChangeStateConstType(_id, _delete);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };

        }
    }
}