﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.ConstValueOperation
{
    public class AddConstValueOperation : BaseOperation<AddConstValueOperation, GetResult<ConstValue>>
    {
        User _user;
        ConstValue _cv;
        public AddConstValueOperation(User user, ConstValue cv)
        {
            _user = user;
            _cv = cv;
        }

        public override GetResult<ConstValue> BeforeOp()
        {
            CompositeValidator validator = new CompositeValidator();
            BaseResult result;
            result = new CheckUser(_user, _cv.User).Validate();
            if (result.ErrorCode != ErrorCode.Success) validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckValidObject(_cv));
            validator.Add(new CheckIsBranchDeleted(_cv.Branch));
            validator.Add(new CheckIdConstType(_cv.ConstType.Id, db));
            validator.Add(new CheckIdUser(_cv.User.Id, db));
            validator.Add(new CheckIdConsTypeIdAndValue(_cv.ConstType.Id, _cv.Value, db));
            result = validator.Validate();
            return new GetResult<ConstValue> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };
        }
        public override GetResult<ConstValue> InOperation()
        {
            _cv = db.InsertConstValue(_cv);
            if (_cv.ConstType.Id != 0)
            {
                return new GetResult<ConstValue>() { Result = _cv };
            }
            else return new GetResult<ConstValue> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}