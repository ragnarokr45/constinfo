﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Models;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.ConstValueOperation
{
    public class UpdateConstValueOperation : BaseOperation<UpdateConstValueOperation, BaseResult>
    {
        User _user;
        ConstValue _cv;
        public UpdateConstValueOperation(User user, ConstValue constValue)
        {
            _user = user;
            _cv = constValue;
        }
        public override BaseResult BeforeOp()
        { 
            CompositeValidator validator = new CompositeValidator();
            BaseResult result;
            result = new CheckUser(_user, _cv.User).Validate();
            if (result.ErrorCode != ErrorCode.Success) validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckConstValueValueById(_cv.ValueId, _cv.Value, db));
            validator.Add(new CheckIsBranchDeleted(_cv.Branch));
            validator.Add(new CheckIdConstValue(_cv.ValueId, db));
            validator.Add(new CheckValidObject(_cv));
            result = validator.Validate();
            return result;

        }
        public override BaseResult InOperation()
        {
            db.UpdateConstValue(_cv);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };

        }
      
    }
}