﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.ConstValueOperation
{
    public class CanUpdateConstValueOperation : BaseOperation<CanUpdateConstValueOperation, BaseResult>
    {
        User _user;
        ConstValue _cv;
        public CanUpdateConstValueOperation(User user, ConstValue cv)
        {
            _user = user;
            _cv = cv;
        }

        public override BaseResult InOperation()
        {
            CompositeValidator validator = new CompositeValidator();
            BaseResult result;
            result = new CheckUser(_user, _cv.User).Validate();
            if (result.ErrorCode != ErrorCode.Success) result = new CheckAdmin(_user.Role).Validate();
            return result;

        }
    }
}