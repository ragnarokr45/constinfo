﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.ConstValueOperation
{
    public class GetOneConstValueOperation : BaseOperation<GetOneConstValueOperation, GetResult<ConstValue>>
    {
        int _id;
        ConstValue _cv;
        public GetOneConstValueOperation(int id)
        {
            _id = id;
        }

        public override GetResult<ConstValue> BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckIdConstValue(_id, db));
            BaseResult result = validator.Validate();
            return new GetResult<ConstValue> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }
        public override GetResult<ConstValue> InOperation()
        {
            _cv = db.GetConstValue(_id);
            if (_cv.ConstType.Id != 0)
            {
                return new GetResult<ConstValue>() { Result = _cv };
            }
            else return new GetResult<ConstValue> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}