﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.ConstValueOperation
{
    public class GetConstValueOperation : BaseOperation<GetConstValueOperation, GetResult<List<ConstValue>>>
    {
        int _id;
        bool _showDeleted;
        public GetConstValueOperation(int id, bool showDeleted)
        {
            _id = id;
            _showDeleted = showDeleted;
        }
        public override GetResult<List<ConstValue>> BeforeOp()
        {
            var validator = new CheckIdConstType(_id, db);
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return new GetResult<List<ConstValue>>() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
        public override GetResult<List<ConstValue>> InOperation()
        {
            return new GetResult<List<ConstValue>>() { Result = db.GetConstValues(_id, _showDeleted) };
        }
    }
}