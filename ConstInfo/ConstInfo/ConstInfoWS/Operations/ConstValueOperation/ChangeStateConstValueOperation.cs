﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Models;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.ConstValueOperation
{
    public class ChangeStateConstValueOperation : BaseOperation<ChangeStateConstValueOperation, BaseResult>
    {
        
        User _user;
        int _constValueId;
        bool _delete;
        public ChangeStateConstValueOperation(User user, int constValueId, bool delete)
        {
            
            _user = user;
            _constValueId = constValueId;
            _delete = delete;
        }

        public override BaseResult BeforeOp()
        {
            CompositeValidator validator = new CompositeValidator();
            BaseResult result;
            result = new CheckIdUsersAndIdConstValue(_user.Id, _constValueId, db).Validate();
            if (result.ErrorCode != ErrorCode.Success) validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckIdConstValue(_constValueId, db));
            result = validator.Validate();
            return result;

        }

        public override BaseResult InOperation()
        {
            db.ChangeStateConstValue(_constValueId, _delete);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };

        }
    }
}
