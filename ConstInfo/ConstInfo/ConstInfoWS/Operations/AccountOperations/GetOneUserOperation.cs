﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Operations.AccountOperations
{
    public class GetOneUserOperation : BaseOperation<GetOneUserOperation, GetResult<User>>
    {
        int _id;
        User _user;
        public GetOneUserOperation(int id)
        {
            _id = id;
        }

         public override GetResult<User> BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckIdUser(_id, db));
            BaseResult result = validator.Validate();
            return new GetResult<User> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }

        public override GetResult<User> InOperation()
        {
            _user = db.GetUser(_id);
            if (_user.Id != 0)
            {
                return new GetResult<User>() { Result = _user };
            }
            else return new GetResult<User> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}