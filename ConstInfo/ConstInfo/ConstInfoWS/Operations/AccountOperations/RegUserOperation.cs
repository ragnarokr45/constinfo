﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using ConstInfoWS.Models;

namespace ConstInfoWS.Operations.AccountOperations
{
    public class RegUserOperation : BaseOperation<RegUserOperation, GetResult<User>>
    {
        private User _currentUser;
        private User _userToAdd;
        public RegUserOperation(User currentUser, User userToAdd)
        {
            _currentUser = currentUser;
            _userToAdd = userToAdd;
        }

        public override GetResult<User> BeforeOp()
        {
           // _userToAdd.Login = _userToAdd.Login.ToUpper();
            _userToAdd.Login = _userToAdd.Login.Trim();
            _userToAdd.FirstName = _userToAdd.FirstName.Trim();
            _userToAdd.SecondName = _userToAdd.SecondName.Trim();
            CompositeValidator validator = new CompositeValidator();
            validator.Add(new CheckIsUserExist(_userToAdd, db));
            validator.Add(new CheckAdmin(_currentUser.Role));
            validator.Add(new CheckValidObject(_userToAdd));
            BaseResult result = validator.Validate();
            return new GetResult<User> { ErrorCode = result.ErrorCode, ErrorDescription = result.ErrorDescription };

        }

        public override GetResult<User> InOperation()
        {
            _userToAdd = db.InsertUser(_userToAdd);
            if (_userToAdd.Id != 0)
            {
                return new GetResult<User>() { Result = _userToAdd };
            }
            else return new GetResult<User> { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }

        
    }
}