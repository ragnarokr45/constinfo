﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;
using ConstInfoWS.Models;

namespace ConstInfoWS.Operations.AccountOperations
{
    public class UpdateUserOperation : BaseOperation<UpdateUserOperation, BaseResult>
    {
        User _currentUser;
        User _userToAdd;
        public UpdateUserOperation(User currentUser, User userToAdd)
        {
            _currentUser = currentUser;
            _userToAdd = userToAdd;
        }

        public override BaseResult BeforeOp()
        {
            _userToAdd.Login = _userToAdd.Login.Trim();
            _userToAdd.FirstName = _userToAdd.FirstName.Trim();
            _userToAdd.SecondName = _userToAdd.SecondName.Trim();
            var validator = new CompositeValidator();
            BaseResult result;
            result = new CheckIsUserExist(_userToAdd, db).Validate();
            if (result.ErrorCode != ErrorCode.Success) validator.Add(new CheckUserLoginById(_userToAdd.Id, _userToAdd.Login, db));
            validator.Add(new CheckAdmin(_currentUser.Role));
            validator.Add(new CheckIdUser(_userToAdd.Id, db));
            validator.Add(new CheckValidObject(_userToAdd));
             result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return result;

        }

        public override BaseResult InOperation()
        {
            db.UpdateUser(_userToAdd);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };

        }
    }
}