﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.AccountOperations
{
    public class ChangeStateUserOperation : BaseOperation<ChangeStateUserOperation, BaseResult>
    {

        User _user;
        int _id;
        bool _delete;
        public ChangeStateUserOperation(int id, User user, bool delete)
        {
            _id = id;
            _user = user;
            _delete = delete;
        }
        public override BaseResult BeforeOp()
        {
            var validator = new CompositeValidator();
            validator.Add(new CheckAdmin(_user.Role));
            validator.Add(new CheckIdUser(_id, db));
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return null;
            else return result;

        }
        public override BaseResult InOperation()
        {
            db.ChangeStateUser(_id, _delete);
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };

        }
    }
}