﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations;
using ConstInfoWS.Result;
using ConstInfoWS.Validators;

namespace ConstInfoWS.Operations.AccountOperations
{
    public class GetUsersOperation : BaseOperation<GetUsersOperation, GetResult<List<User>>>
    {
        bool _showDeleted;
        User _user;

        public GetUsersOperation(bool showDeleted,User user)
        {
            _showDeleted = showDeleted;
            _user = user;
        }

        public override GetResult<List<User>> BeforeOp()
        {
            var validator = new CheckAdmin(_user.Role);
            BaseResult result = validator.Validate();
            if (result.ErrorCode == ErrorCode.Success) return new GetResult<List<User>> {  ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new GetResult<List<User>>() { ErrorCode = ErrorCode.RoleError, ErrorDescription = "You don't have permissions to do that" };
        }

        public override GetResult<List<User>> InOperation()
        {
            return new GetResult<List<User>>() { Result = db.GetUsers(_showDeleted) };
        }
    }
}