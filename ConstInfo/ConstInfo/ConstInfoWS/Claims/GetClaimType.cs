﻿using ConstInfoWS.DB_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace ConstInfoWS.Claims
{
    public class GetClaimType
    {
       


        public User GetUserWithClaims(IPrincipal User)
        {
            ClaimsIdentity claimsIdentity =  User.Identity as ClaimsIdentity;
            Claim claimRole = claimsIdentity?.FindFirst(ClaimTypes.Role);
            Claim claimId = claimsIdentity?.FindFirst(CustomClaimTypes.UID);

            Roles role = (Roles)Enum.Parse(typeof(Roles), claimRole?.Value, true);
            int uid = Int32.Parse(claimId.Value);
            User user = new User() { Id = uid, Role = role };
            return user;
        }
    }
}