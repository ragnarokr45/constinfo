﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Claims
{
    public static class CustomClaimTypes
    {
        public const string UID = "UID";
        public const string sub = "sub";
        public const string isDeleted = "isDeleted";
    }
}