﻿using ConstInfoWS.DB_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Models
{
    public class AdminUserModel
    {
        public User Admin { get; set; }
        public User User { get; set; }
    }
}