﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Models
{
    public class UserConstValueModel
    {
        public User User { get; set; }
        public ConstValue ConstValue { get; set; }
    }
}