﻿using ConstInfoWS.DB_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Models
{
    public class UserConstTypeModel
    {
        public User User { get; set; }
        public ConstType ConstType { get; set; }
    }
}