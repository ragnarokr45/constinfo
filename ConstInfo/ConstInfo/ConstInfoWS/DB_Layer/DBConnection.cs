﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web.Configuration;

namespace ConstInfoWS.DB_Layer
{

    public class DBConnection : IDisposable
    {
        public void Dispose()
        {
            connection.Close();
            GC.SuppressFinalize(this);
        }

        //~DBConnection()
        //{
        //    Dispose();
        //}

        private SqlConnection connection;
        private string connectionString;

        public DBConnection()
        {
            connectionString = WebConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
            // connectionString = @"Server=PC_1;Database=ConstInfoDB;User Id=sa;Password=upitec;Trusted_Connection=True;Connection Timeout = 2;Min Pool Size = 5; Max Pool Size = 60;";
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            catch(Exception ex)
            {
               string e = ex.ToString();
                connectionString = connectionString + "Pooling = false;";
                
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            
        }

        #region Check methods

        public bool DBCheckId(int constTypeId, string[] values)
        {
            string query = $"select {values[1]} from {values[0]} where {values[1]} = @id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@id", constTypeId);

                object r = cmd.ExecuteScalar();

                return r != null && !(r is DBNull);
            }
        }


        public bool CheckIdConstType(int constTypeId)
        {
            string query = "select const_id from Const_Types where const_id = @id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@id", constTypeId);

                object r = cmd.ExecuteScalar();

                return r != null && !(r is DBNull);
            }
        }

        public bool CheckIdBranch(int branchId)
        {
            string query = "select branch_id from Branch where branch_id = @id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@id", branchId);

                object r = cmd.ExecuteScalar();

                return r != null && !(r is DBNull);
            }
        }

        public bool CheckIdConstValue(int valueId)
        {
            string query = "select value_id from Const_Values where value_id = @value_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@value_id", valueId);
                object r = cmd.ExecuteScalar();
                return r != null && !(r is DBNull);
            }
        }
        public bool CheckIdConsTypeIdAndValue(int constTypeId, string value)
        {
            string query = "select value_id from Const_Values where const_id = @const_id and value_= @value_";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@const_id", constTypeId);
                cmd.Parameters.AddWithValue("@value_", value);
                object r = cmd.ExecuteScalar();
                return r != null && !(r is DBNull);
            }
        }
        public bool CheckIdUsersAndIdConstTypeAndValue(int constTypeId, int userId, string value)
        {
            string query = "select user_id from Const_Values where value_ = @value_ and user_id=@user_id and const_id = @const_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@value_", value);
                cmd.Parameters.AddWithValue("@user_id", userId);
                cmd.Parameters.AddWithValue("@const_id", constTypeId);
                object r = cmd.ExecuteScalar();
                return r != null && !(r is DBNull);
            }
        }
        public bool CheckIdUsersAndIdConstValue(int valueId, int userId)
        {
            string query = "select value_id from Const_Values where value_id = @value_id and user_id=@user_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@value_id", valueId);
                cmd.Parameters.AddWithValue("@user_id", userId);
                object r = cmd.ExecuteScalar();
                return r != null && !(r is DBNull);
            }
        }
        public bool CheckIdUser(int userId)
        {
            string query = "select user_id from Users where user_id = @id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@id", userId);

                object r = cmd.ExecuteScalar();

                return r != null && !(r is DBNull);
            }
        }
        //kek
        public bool IsLoginExist(string userLogin)
        {
            string query = "select user_login from Users where user_login = @user_login";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@user_login", userLogin);

                object r = cmd.ExecuteScalar();

                return r != null && !(r is DBNull);
            }
        }
        public bool IsUserDeleted(int userId)
        {
            string query = "select user_id from Users where user_id = @id and isDeleted = 1";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@id", userId);

                object r = cmd.ExecuteScalar();

                return r != null && !(r is DBNull);
            }

        }
        #endregion


        #region SELECT methods

        public List<ConstType> GetConstTypes(bool showDeleted)
        {
            string select;
            List<ConstType> constTypes = new List<ConstType>();
            if (showDeleted)

            {
                select = "select const_id, const_name, isDeleted from Const_Types";
            }
            else
            {
                select = "select const_id, const_name, isDeleted from Const_Types where isDeleted = 0";
            }
            using (SqlCommand cmd = new SqlCommand(select, connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int cnt = 0;
                        var _constType = new ConstType();
                        _constType.Id = reader.GetInt32(cnt++);
                        _constType.ConstName = reader.GetString(cnt++);
                        _constType.IsDeleted = reader.GetBoolean(cnt++);
                        constTypes.Add(_constType);
                    }
                }
            }

            return constTypes;
        }
        public ConstType GetConstType(int constTypeId)
        {
            string select;
            ConstType constType = null;
           
                select = "select const_id, const_name, isDeleted from Const_Types where const_id = "+constTypeId;
            
            using (SqlCommand cmd = new SqlCommand(select, connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int cnt = 0;
                        constType = new ConstType();
                        constType.Id = reader.GetInt32(cnt++);
                        constType.ConstName = reader.GetString(cnt++);
                        constType.IsDeleted = reader.GetBoolean(cnt++);
                      
                    }
                }
            }

            return constType;
        }
        public List<Branch> GetBranchs(bool showDeleted)
        {
            string select;
            List<Branch> branch = new List<Branch>();
            if (showDeleted)

            {
                select = "select branch_id, branch_name, isDeleted from Branch";
            }
            else
            {
                select = "select branch_id, branch_name, isDeleted from Branch where isDeleted = 0";
            }
            using (SqlCommand cmd = new SqlCommand(select, connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int cnt = 0;
                        var _branch = new Branch();
                        _branch.Id = reader.GetInt32(cnt++);
                        _branch.BranchName = reader.GetString(cnt++);
                        _branch.IsDeleted = reader.GetBoolean(cnt++);
                        branch.Add(_branch);
                    }
                }
            }

            return branch;
        }
        public Branch GetBranch(int branchId)
        {
            string select;
         Branch branch = null;
          
                select = "select branch_id, branch_name, isDeleted from Branch where branch_id = " + branchId;
     
            using (SqlCommand cmd = new SqlCommand(select, connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int cnt = 0;
                        branch = new Branch();
                        branch.Id = reader.GetInt32(cnt++);
                        branch.BranchName = reader.GetString(cnt++);
                        branch.IsDeleted = reader.GetBoolean(cnt++);
                    }
                }
            }

            return branch;
        }
        public List<ConstValue> GetConstValues(int constTypeId, bool showDeleted)
        {
            List<ConstValue> constValues = new List<ConstValue>();

            string query;
            if (showDeleted)
            {
                query = @"select  ct.const_id, date_, name_, value_, br.branch_id, br.branch_name, br.isDeleted, remark, 
                                u.user_id, u.user_login, u.first_name, u.second_name, u.user_role, 
                                ct.const_name, cv.isDeleted, cv.value_id
								from Const_Values cv
								inner join Users u on cv.user_id = u.user_id
								inner join Const_Types ct on cv.const_id = ct.const_id
                                inner join Branch br on cv.branch_id = br.branch_id
                                where cv.const_id = @const_id";
            }
            else
            {
                query = @"select ct.const_id, date_, name_, value_, br.branch_id, br.branch_name, br.isDeleted, remark,
                                u.user_id, u.user_login, u.first_name, u.second_name, u.user_role,
                                ct.const_name, cv.isDeleted, cv.value_id
								from Const_Values cv
								inner join Users u on cv.user_id = u.user_id
								inner join Const_Types ct on cv.const_id = ct.const_id
                                inner join Branch br on cv.branch_id = br.branch_id
                                where cv.const_id = @const_id  and cv.isDeleted = 0";
            }


            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@const_id", constTypeId);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int cnt = 0;
                        var constValue = new ConstValue() { User = new User(), ConstType = new ConstType(), Branch = new Branch() };
                       
                        constValue.ConstType.Id = reader.GetInt32(cnt++);
                        constValue.Date = reader.GetDateTime(cnt++);
                        constValue.Name = reader.GetString(cnt++);
                        constValue.Value = reader.GetString(cnt++);
                        constValue.Branch.Id = reader.GetInt32(cnt++);
                        constValue.Branch.BranchName = reader.GetString(cnt++);
                        constValue.Branch.IsDeleted = reader.GetBoolean(cnt++);
                        constValue.Remark = reader.GetString(cnt++);
                        constValue.User.Id = reader.GetInt32(cnt++);
                        constValue.User.Login = reader.GetString(cnt++);
                        constValue.User.FirstName = reader.GetString(cnt++);
                        constValue.User.SecondName = reader.GetString(cnt++);
                        constValue.User.Role = (Roles)reader.GetInt32(cnt++);
                        constValue.ConstType.ConstName = reader.GetString(cnt++);
                        constValue.IsDeleted = reader.GetBoolean(cnt++);
                        constValue.ValueId = reader.GetInt32(cnt++);
                        constValues.Add(constValue);
                    }
                }
            }

            return constValues;
        }
        public ConstValue GetConstValue(int valueId)
        {
           ConstValue constValue = null;

            string query = @"select ct.const_id, date_, name_, value_, br.branch_id, br.branch_name, br.isDeleted, remark,
                                u.user_id, u.user_login, u.first_name, u.second_name, u.user_role,
                                ct.const_name, cv.isDeleted, cv.value_id
								from Const_Values cv
								inner join Users u on cv.user_id = u.user_id
								inner join Const_Types ct on cv.const_id = ct.const_id
                                inner join Branch br on cv.branch_id = br.branch_id
                                where cv.value_id = "+valueId;
            
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@valueId", valueId);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int cnt = 0;
                        constValue = new ConstValue() { User = new User(), ConstType = new ConstType(), Branch = new Branch() };

                        constValue.ConstType.Id = reader.GetInt32(cnt++);
                        constValue.Date = reader.GetDateTime(cnt++);
                        constValue.Name = reader.GetString(cnt++);
                        constValue.Value = reader.GetString(cnt++);
                        constValue.Branch.Id = reader.GetInt32(cnt++);
                        constValue.Branch.BranchName = reader.GetString(cnt++);
                        constValue.Branch.IsDeleted = reader.GetBoolean(cnt++);
                        constValue.Remark = reader.GetString(cnt++);
                        constValue.User.Id = reader.GetInt32(cnt++);
                        constValue.User.Login = reader.GetString(cnt++);
                        constValue.User.FirstName = reader.GetString(cnt++);
                        constValue.User.SecondName = reader.GetString(cnt++);
                        constValue.User.Role = (Roles)reader.GetInt32(cnt++);
                        constValue.ConstType.ConstName = reader.GetString(cnt++);
                        constValue.IsDeleted = reader.GetBoolean(cnt++);
                        constValue.ValueId = reader.GetInt32(cnt++);
                    }
                }
            }

            return constValue;
        }

        public List<User> GetUsers(bool showDeleted)
        {
            List<User> users = new List<User>();


            string query;
            if (showDeleted)
            {
                query = @"select user_id, user_login, first_name, second_name, user_role, isDeleted
                                from Users";
            }
            else
            {
                query = @"select user_id, user_login, first_name, second_name, user_role, isDeleted
                                from Users where isDeleted = 0";
            }
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {


                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int cnt = 0;
                        var user = new User();
                        user.Id = reader.GetInt32(cnt++);
                        user.Login = reader.GetString(cnt);
                        if(!reader.IsDBNull(++cnt))
                        user.FirstName = reader.GetString(cnt);
                        if (!reader.IsDBNull(++cnt))
                        user.SecondName = reader.GetString(cnt);
                        user.Role = (Roles)reader.GetInt32(++cnt);
                        user.IsDeleted = reader.GetBoolean(++cnt);
                        users.Add(user);
                    }
                }
            }

            return users;
        }

        public User GetUser(int userId)
        {
           User user = null;
          string  query = @"select user_id, user_login, first_name, second_name, user_role, isDeleted
                                from Users where user_id = "+userId;
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        int cnt = 0;
                        user = new User();
                        user.Id = reader.GetInt32(cnt++);
                        user.Login = reader.GetString(cnt);
                        if (!reader.IsDBNull(++cnt))
                            user.FirstName = reader.GetString(cnt);
                        if (!reader.IsDBNull(++cnt))
                            user.SecondName = reader.GetString(cnt);
                        user.Role = (Roles)reader.GetInt32(++cnt);
                        user.IsDeleted = reader.GetBoolean(++cnt);
                       
                    }
                }
            }

            return user;
        }
        #endregion

        public User UserLogin(string login, string password)
        {
            User user = new User();

            string query = @"select user_id, user_login, first_name, second_name, user_role, isDeleted
                                from Users
                                where   BINARY_CHECKSUM(user_login) = BINARY_CHECKSUM(@login) and BINARY_CHECKSUM(user_password) = BINARY_CHECKSUM(@password)";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@login", login);
                cmd.Parameters.AddWithValue("@password", password);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int cnt = 0;
                        user.Id = reader.GetInt32(cnt++);
                        user.Login = reader.GetString(cnt++);
                        user.FirstName = reader.GetString(cnt++);
                        user.SecondName = reader.GetString(cnt++);
                        user.Role = (Roles)reader.GetInt32(cnt++);
                        user.IsDeleted = reader.GetBoolean(cnt++);
                    }
                }
            }
            return user;
        }

        #region INSERT methods

        public User InsertUser(User user)

        {

            string query = @"insert into Users(user_login, first_name, second_name, user_role, user_password, isDeleted)
                                values(@user_login, @first_name, @second_name, @user_role, @user_password, @isDeleted)";
            using (SqlTransaction transaction = connection.BeginTransaction())
            {
                try
                {
                    using (SqlCommand cmdInsert = new SqlCommand(query, connection, transaction))
                    {
                        cmdInsert.Parameters.AddWithValue("@user_login", user.Login.Trim());
                        cmdInsert.Parameters.AddWithValue("@first_name", user.FirstName?.Trim() ?? SqlString.Null);
                        cmdInsert.Parameters.AddWithValue("@second_name", user.SecondName?.Trim() ?? SqlString.Null);
                        cmdInsert.Parameters.AddWithValue("@user_role", (int)user.Role);
                        cmdInsert.Parameters.AddWithValue("@user_password", user.Password ?? SqlString.Null);
                        cmdInsert.Parameters.AddWithValue("@isDeleted", user.IsDeleted);
                        cmdInsert.ExecuteNonQuery();
                    }
                    using (SqlCommand cmdGetId = new SqlCommand("SELECT IDENT_CURRENT('Users')", connection, transaction))
                    {
                        Decimal d = (decimal)cmdGetId.ExecuteScalar();
                        user.Id = Decimal.ToInt32(d);

                    }
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
            }
            return user;
        }

        public ConstType InsertConstType(ConstType constType)
        {
            string query = "insert into Const_Types(const_name, isDeleted) values(@const_name, @isDeleted)";
            using (SqlTransaction transaction = connection.BeginTransaction())
            {
                try
                {
                    using (SqlCommand cmdInsert = new SqlCommand(query, connection, transaction))
                    {
                        cmdInsert.Parameters.AddWithValue("@const_name", constType.ConstName.Trim());
                        cmdInsert.Parameters.AddWithValue("@isDeleted", constType.IsDeleted);
                        cmdInsert.ExecuteNonQuery();
                    }
                    using (SqlCommand cmdGetId = new SqlCommand("SELECT IDENT_CURRENT('Const_Types')", connection, transaction))
                    {
                        Decimal d = (decimal)cmdGetId.ExecuteScalar();
                        constType.Id = Decimal.ToInt32(d);
                    }
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }

            }
            return constType;

        }
        public Branch InsertBranch(Branch branch)
        {
            string query = "insert into Branch(branch_name, isDeleted) values(@branch_name, @isDeleted)";
            using (SqlTransaction transaction = connection.BeginTransaction())
            {
                try
                {
                    using (SqlCommand cmdInsert = new SqlCommand(query, connection, transaction))
                    {
                        cmdInsert.Parameters.AddWithValue("@branch_name", branch.BranchName.Trim());
                        cmdInsert.Parameters.AddWithValue("@isDeleted", branch.IsDeleted);
                        cmdInsert.ExecuteNonQuery();
                    }
                    using (SqlCommand cmdGetId = new SqlCommand("SELECT IDENT_CURRENT('Branch')", connection, transaction))
                    {
                        Decimal d = (decimal)cmdGetId.ExecuteScalar();
                        branch.Id = Decimal.ToInt32(d);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }

            }
            return branch;

        }

        public ConstValue InsertConstValue(ConstValue constValue)
        {
                string query = @"insert into Const_Values(const_id, user_id, date_, name_, value_, branch_id, remark, isDeleted)
                                values(@const_id, @user_id,  @date_, @name_, @value_, @branch_id, @remark, @isDeleted)";
            using (SqlTransaction transaction = connection.BeginTransaction())
            {
                try
                {
                    using (SqlCommand cmdInsert = new SqlCommand(query, connection, transaction))
                    {
                        cmdInsert.Parameters.AddWithValue("@const_id", constValue.ConstType.Id);
                        cmdInsert.Parameters.AddWithValue("@user_id", constValue.User.Id);
                        cmdInsert.Parameters.AddWithValue("@date_", constValue.Date);
                        cmdInsert.Parameters.AddWithValue("@name_", constValue.Name.Trim());
                        cmdInsert.Parameters.AddWithValue("@value_", constValue.Value.Trim());
                        cmdInsert.Parameters.AddWithValue("@branch_id", constValue.Branch.Id);
                        cmdInsert.Parameters.AddWithValue("@remark", constValue.Remark.Trim());
                        cmdInsert.Parameters.AddWithValue("@isDeleted", constValue.IsDeleted);
                        cmdInsert.ExecuteNonQuery();
                    }
                    using (SqlCommand cmdGetId = new SqlCommand("SELECT IDENT_CURRENT('Const_Values')", connection, transaction))
                    {
                        Decimal d = (decimal)cmdGetId.ExecuteScalar();
                        constValue.ValueId = Decimal.ToInt32(d);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
            }

                return constValue;
        }
        #endregion

        #region DELETE methods
        public void ChangeStateUser(int userId, bool delete)
        {
            string query;
            if (delete)
                query = "UPDATE Users SET isDeleted = 1 where user_id = @user_id";
            else
            {
                query = "UPDATE Users SET isDeleted = 0 where user_id = @user_id";
            }
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@user_id", userId);
                cmd.ExecuteNonQuery();
            }
        }

        public void ChangeStateConstType(int constTypeId, bool delete)
        {
            string query;
            if (delete)
                query = "UPDATE Const_Types SET isDeleted = 1 where const_id = @const_id";
            else
            {
                query = "UPDATE Const_Types SET isDeleted = 0 where const_id = @const_id";
            }
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@const_id", constTypeId);
                cmd.ExecuteNonQuery();
            }
        }
        public void ChangeStateBranch(int branchId, bool delete)
        {
            string query;
            if (delete)
                query = "UPDATE Branch SET isDeleted = 1 where branch_id = @branch_id";
            else
                query = "UPDATE Branch SET isDeleted = 0 where branch_id = @branch_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@branch_id", branchId);
                cmd.ExecuteNonQuery();
            }
        }
        public void ChangeStateConstValue(int valueId, bool delete)
        {
            string query;
            if (delete)
                query = "UPDATE Const_Values SET isDeleted = 1 where value_id = @value_id";
            else
                query = "UPDATE Const_Values SET isDeleted = 0 where value_id = @value_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@value_id", valueId);
                cmd.ExecuteNonQuery();
            }

        }
        #endregion

        #region UPDATE methods
        public void UpdateUser(User user)
        {
            string query = @"update Users set ";
            string z = "";
            if (user.Login != null)
                z = z + "user_login = @user_login";
            if (user.FirstName != null)
            {
                if (z != "")
                {
                    z = z + ", ";
                }
                z = z + "first_name = @first_name";
            }
            if (user.SecondName != null)
            {
                if (z != "")
                {
                    z = z + ", ";
                }
                z = z + "second_name = @second_name";
            }
            if (user.Password != null)
            {
                if (z != "")
                {
                    z = z + ", ";
                }
                z = z + "user_password = @user_password";
            }
            if (user.Role != 0)
            {
                if (z != "")
                {
                    z = z + ", ";
                }
                z = z + "user_role = @user_role";
            }
            query = query+ z + "  where user_id = @user_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.AddWithValue("@user_id", user.Id);
                    cmd.Parameters.AddWithValue("@user_login", user.Login?.Trim() ?? SqlString.Null);
                    cmd.Parameters.AddWithValue("@first_name", user.FirstName?.Trim() ?? SqlString.Null);
                    cmd.Parameters.AddWithValue("@second_name", user.SecondName?.Trim() ?? SqlString.Null);
                    cmd.Parameters.AddWithValue("@user_password", user.Password?.Trim() ?? SqlString.Null);
                    cmd.Parameters.AddWithValue("@user_role", (int)user.Role);
                    cmd.ExecuteNonQuery();
                }
        }

        public void UpdateConstType(ConstType constType)
        {
                string query = @"update Const_Types set const_name = @const_name where const_id = @const_id";
                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.AddWithValue("@const_id", constType.Id);
                    cmd.Parameters.AddWithValue("@const_name", constType.ConstName?.Trim() ?? SqlString.Null);
                    cmd.ExecuteNonQuery();
                }
        }
        public void UpdateBranch(Branch branch)
        {
            string query = @"update Branch set branch_name = @branch_name where branch_id = @branch_id";
            using (SqlCommand cmd = new SqlCommand(query, connection))
            {
                cmd.Parameters.AddWithValue("@branch_id", branch.Id);
                cmd.Parameters.AddWithValue("@branch_name", branch.BranchName?.Trim() ?? SqlString.Null);
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateConstValue(ConstValue constValue)
        {
            string query = @"update Const_Values set ";
            string add = "";
            if(constValue.User !=null)
            {
                add = add + "user_id =  @user_id ";
            }
            if (constValue.Date.HasValue)
            {
                if (add != "")
                {
                    add = add + ", ";
                }
                add = add + "const_id = @const_id ";
            }
            if (constValue.Date.HasValue)
            {
                if(add!="")
                {
                    add = add + ", ";
                }
                add = add + "date_ = @date_ ";
            }
            if (constValue.Value != null)
            {
                if (add != "")
                {
                    add = add + ", ";
                }
                add = add + "value_ = @value_ ";
            }
            if (constValue.Name!=null)
            {
                if (add != "")
                {
                    add = add + ", ";
                }
                add = add + "name_ = @name_ ";
            }       
            if (constValue.Branch!=null)
            {
                if (add != "")
                {
                    add = add + ", ";
                }
                add = add + "branch_id = @branch ";
            }
            if (constValue.Remark!=null)
            {
                if (add != "")
                {
                    add = add + ", ";
                }
                add = add + "remark = @remark ";
            }
         
               query=query+add+" where value_id = @value_id";
            using (SqlCommand cmd = new SqlCommand(query, connection)) 
            {
                cmd.Parameters.AddWithValue("@const_id", constValue.ConstType.Id);
                cmd.Parameters.AddWithValue("@user_id", constValue.User?.Id ?? SqlInt32.Null);
                cmd.Parameters.AddWithValue("@date_", constValue.Date ?? SqlDateTime.Null);
                cmd.Parameters.AddWithValue("@name_", constValue.Name?.Trim() ?? SqlString.Null);
                cmd.Parameters.AddWithValue("@value_", constValue.Value.Trim());
                cmd.Parameters.AddWithValue("@branch", constValue.Branch?.Id ?? SqlInt32.Null);
                cmd.Parameters.AddWithValue("@remark", constValue.Remark?.Trim() ?? SqlString.Null);
                cmd.Parameters.AddWithValue("@value_id", constValue.ValueId);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion
    }

}