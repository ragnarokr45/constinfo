﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstInfoWS.DB_Layer
{
    public class ConstType
    {
        public int Id { get; set; }
        [Required, MaxLength(350)]
        public string ConstName { get; set; }
    
        public bool IsDeleted { get; set; }
    }
}