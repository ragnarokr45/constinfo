﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstInfoWS.DB_Layer
{
    public class Branch
    {
        public int Id { get; set; }
        [Required, MaxLength(30)]
        public string BranchName { get; set; }

        public bool IsDeleted { get; set; }
    }
}