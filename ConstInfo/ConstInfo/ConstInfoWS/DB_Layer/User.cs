﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ConstInfoWS.DB_Layer
{
    public class User
    {
        public int Id { get; set; }
        [MinLength(3), MaxLength(50)]
        public string Login { get; set; }
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string SecondName { get; set; }
        [MaxLength(100)]
        public string Password { get; set; }
        [Required, Range(1, 2)]
        public Roles Role { get; set; }
    
        public bool IsDeleted { get; set; }
    }

    
}