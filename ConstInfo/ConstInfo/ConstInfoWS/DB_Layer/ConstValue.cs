﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstInfoWS.DB_Layer
{
    public class ConstValue
    {
        public int ValueId { get; set; }
        public ConstType ConstType { get; set; }
        public User User { get; set; }
        [Required]
        public DateTime? Date { get; set; }
        [Required, MaxLength(100)]
        public string Name { get; set; }
        [Required, MaxLength(25)]
        public string Value { get; set; }
        [Required]
        public Branch Branch { get; set; }
        [MaxLength(300)]
        public string Remark { get; set; }
        public bool IsDeleted { get; set; }
       
    }
}