﻿using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CompositeValidator : IValidate
    {
        List<IValidate> list = new List<IValidate>();
        public BaseResult Validate()
        {
            BaseResult result;
            foreach (var o in list)
            {
                result = o.Validate();
                if(result.ErrorCode != ErrorCode.Success) return result ;
            }
            return result = new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
        }

        internal void Add(IValidate obj)
        {
            list.Add(obj);
        }
    }
}