﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Validators
{
    public class CheckIsUserExist : IValidate
    {
        User _user;
        DBConnection _db;
        public CheckIsUserExist(User user, DBConnection db)
        {
            _user = user;
            _db = db;
        }

        public BaseResult Validate()
        {
            if (!_db.IsLoginExist(_user.Login)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.LoginError, ErrorDescription = "This user alredy exist" };
        }
    }
}