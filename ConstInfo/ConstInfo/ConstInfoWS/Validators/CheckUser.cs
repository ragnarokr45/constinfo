﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckUser : IValidate
    {
        User _user1;
        User _user2;
        public CheckUser(User user1, User user2)
        {
            _user1 = user1;
            _user2 = user2;
        }
        public BaseResult Validate()
        {
            if (_user1.Id == _user2.Id)
            {
                return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            }
            return new BaseResult() { ErrorCode = ErrorCode.RoleError, ErrorDescription = "You don't have permissions to do that" };
        }
    }
}