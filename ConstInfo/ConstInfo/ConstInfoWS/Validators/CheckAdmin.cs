﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckAdmin:IValidate
    {
        Roles _role;
        public CheckAdmin(Roles role)
        {
            _role = role;
        }
        public BaseResult Validate()
        {
            if (_role == Roles.admin)
            {
                return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            }
            return  new BaseResult() { ErrorCode = ErrorCode.RoleError, ErrorDescription = "You don't have permissions to do that" };
        }
    }
}