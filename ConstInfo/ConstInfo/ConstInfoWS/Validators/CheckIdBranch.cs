﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Validators
{
    public class CheckIdBranch : IValidate
    {
        int _branchId;
        DBConnection _db;
        public CheckIdBranch(int branchId, DBConnection db)
        {
            _branchId = branchId;
            _db = db;
        }
        public BaseResult Validate()
        {
            if (_db.CheckIdBranch(_branchId)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}