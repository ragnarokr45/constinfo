﻿using ConstInfoWS.DB_Layer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;

namespace ConstInfoWS.Validators
{
    public class CheckIdUser : IValidate
    {
        int _userId;
        DBConnection _db;
        public CheckIdUser(int userId, DBConnection db)
        {
            _userId = userId;
            _db = db;
        }

        public BaseResult Validate()
        {
            if(_db.CheckIdUser(_userId)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };

        }
    }
}