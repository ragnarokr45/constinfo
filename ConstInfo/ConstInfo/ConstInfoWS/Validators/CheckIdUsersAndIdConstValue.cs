﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckIdUsersAndIdConstValue : IValidate
    {
        int _userId;
        int _valueId;
        DBConnection _db;
        public CheckIdUsersAndIdConstValue(int userId, int valueId, DBConnection db)
        {
            _userId = userId;
            _valueId = valueId;
            _db = db;
        }

        public BaseResult Validate()
        {
            var result = _db.CheckIdUsersAndIdConstValue(_valueId, _userId);
            if (result) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}