﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckIdConstValue : IValidate
    {
        int _constValueId;
        DBConnection _db;

        public CheckIdConstValue(int constValueId, DBConnection db)
        {
            _constValueId = constValueId;
            _db = db;
            
        }

        public BaseResult Validate()
        {
            if (_db.CheckIdConstValue(_constValueId))  return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" }; 
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}
