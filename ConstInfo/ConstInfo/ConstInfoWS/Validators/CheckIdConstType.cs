﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckIdConstType : IValidate
    {
        int _constTypeId;
        DBConnection _db;

        public CheckIdConstType(int constTypeId, DBConnection db)
        {
            _constTypeId = constTypeId;
            _db = db;
        }

        public BaseResult Validate()
        { 
            if (_db.CheckIdConstType(_constTypeId)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}