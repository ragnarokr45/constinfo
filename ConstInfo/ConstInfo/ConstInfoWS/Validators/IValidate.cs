﻿using ConstInfoWS.DB_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfoWS.Result;

namespace ConstInfoWS.Validators
{
    interface IValidate
    {
        BaseResult Validate();
    }
}
