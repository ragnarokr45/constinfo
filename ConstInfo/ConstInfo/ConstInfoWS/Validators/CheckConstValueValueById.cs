﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Validators
{
    public class CheckConstValueValueById : IValidate
    {
        int _id;
        string _value;
        DBConnection _db;


        public CheckConstValueValueById(int id, string value, DBConnection db)
        {
            _id = id;
            _value = value;
            _db = db;
        }
        public BaseResult Validate()
        {
            if (_value == _db.GetConstValue(_id).Value) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.CustomError, ErrorDescription = "This value alredy exist" };
        }
    }
}