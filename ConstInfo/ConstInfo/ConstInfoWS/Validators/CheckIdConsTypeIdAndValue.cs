﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Validators
{
    public class CheckIdConsTypeIdAndValue : IValidate
    {
        int _constTypeId;
        string _value;
        DBConnection _db;
        public CheckIdConsTypeIdAndValue(int constTypeId, string value, DBConnection db)
        {
            _constTypeId = constTypeId;
            _value = value;
            _db = db;
        }

        public BaseResult Validate()
        {
            if (!_db.CheckIdConsTypeIdAndValue(_constTypeId, _value)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}