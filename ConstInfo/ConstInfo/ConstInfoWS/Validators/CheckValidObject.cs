﻿using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckValidObject : IValidate
    {
        object _obj;
        public CheckValidObject(object obj)
        {
            _obj = obj;
        }
        public BaseResult Validate()
        {
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(_obj, new ValidationContext(_obj), result, true))
            {
                return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
            }
            return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
        }
    }
}