﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Validators
{
    public class CheckIsUserDeleted : IValidate
    {
        int _id;
        DBConnection _db;

        public CheckIsUserDeleted(int id, DBConnection db)
        {
            _id = id;
            _db = db;
        }
        public BaseResult Validate()
        {
            if (!_db.IsUserDeleted(_id)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.UserExistenceError, ErrorDescription = "This user has been deleted" };
        }
    }
}