﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckIdUsersAndIdConstTypeAndValue : IValidate
    {
        int _userId;
        string _value;
        int _constTypeId;
        DBConnection _db;
        public CheckIdUsersAndIdConstTypeAndValue(int constTypeId, int userId, string value, DBConnection db)
        {
            _userId = userId;
            _constTypeId = constTypeId;
            _value = value;
            _db = db;
        }

        public BaseResult Validate()
        {
            if (_db.CheckIdUsersAndIdConstTypeAndValue(_constTypeId, _userId, _value)) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.InputError, ErrorDescription = "Invalid input data" };
        }
    }
}