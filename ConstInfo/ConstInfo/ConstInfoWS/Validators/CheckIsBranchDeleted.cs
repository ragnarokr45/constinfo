﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;
using ConstInfoWS.DB_Layer;

namespace ConstInfoWS.Validators
{
    public class CheckIsBranchDeleted : IValidate
    {
        Branch _branch;

        public CheckIsBranchDeleted(Branch branch)
        {
            _branch = branch;
        }

        public BaseResult Validate()
        {
           if(_branch.IsDeleted) return new BaseResult() { ErrorCode = ErrorCode.CustomError, ErrorDescription = "This branch has been deleted" };
           else return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
        }
    }
}