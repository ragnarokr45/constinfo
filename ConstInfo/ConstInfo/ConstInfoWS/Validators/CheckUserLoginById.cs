﻿using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Validators
{
    public class CheckUserLoginById : IValidate
    {
        int _id;
        string _login;
        DBConnection _db;

        public CheckUserLoginById(int id, string login, DBConnection db)
        {
            _id = id;
            _login = login;
            _db = db;
        }
        public BaseResult Validate()
        {
            if (_login == _db.GetUser(_id).Login) return new BaseResult() { ErrorCode = ErrorCode.Success, ErrorDescription = "Success" };
            else return new BaseResult() { ErrorCode = ErrorCode.LoginError, ErrorDescription = "This user alredy exist" };
        }
    }
}