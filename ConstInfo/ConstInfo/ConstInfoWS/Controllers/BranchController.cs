﻿using ConstInfoWS.Authorize;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations.BranchOperations;
using ConstInfoWS.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConstInfoWS.Controllers
{
    [CustomAuthorize]
    [RoutePrefix("branch")]
    public class BranchController : ApiController
    {

        [HttpGet]
        [Route("branches")]
        public GetResult<List<Branch>> GetBranches([FromUri] bool showDeleted = false)
        {
            return GetBranchOperation.Execute(showDeleted);
        }


        [HttpGet]
        [Route("onebranch")]
        public BaseResult GetOneBranch([FromUri] int id)
        {
            return GetOneBranchOperation.Execute(id);
        }

        [HttpPost]
        [Route("add")]
        public BaseResult AddBranches([FromBody] Branch branch)
        {
          
            return AddBranchOperation.Execute(branch);
        }

        [HttpPut]
        [Route("update")]
        public BaseResult UpdateBranches([FromBody] Branch branch)
        {
           
            return UpdateBranchOperation.Execute(branch);
        }

        [HttpDelete]
        [Route("changestate")]
        public BaseResult ChangeStateBranches([FromUri] int id, bool delete)
        {
            
            return ChangeStateBranchOperation.Execute(id, delete);
        }
    }
}
