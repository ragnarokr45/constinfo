﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations.AccountOperations;
using ConstInfoWS.Result;
using ConstInfoWS.Attributes;
using ConstInfoWS.Authorize;
using System.Web.Http.Controllers;
using ConstInfoWS.Models;
using System.Security.Claims;
using ConstInfoWS.Claims;


namespace ConstInfoWS.Controllers
{
    [CustomAuthorize(Roles = "admin")]
    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        [HttpGet]
        [Route("users")]
        public GetResult<List<User>> GetUsers([FromUri] bool showDeleted)
        {
            User currentUser = new GetClaimType().GetUserWithClaims(User);
            return GetUsersOperation.Execute(showDeleted, currentUser);
        }

        [HttpGet]
        [Route("oneuser")]
        public BaseResult GetOneUser([FromUri] int id)
        {
            return GetOneUserOperation.Execute(id);
        }

        [HttpPost]
        [Route("reguser")]
        public BaseResult RegUser([FromBody]User user)
        {
            User currentUser = new GetClaimType().GetUserWithClaims(User);
            return RegUserOperation.Execute(currentUser, user);
        }

        [HttpPut]
        [Route("update")]
        public BaseResult UpdateUser([FromBody] User user)
        {
            User currentUser = new GetClaimType().GetUserWithClaims(User);
            return UpdateUserOperation.Execute(currentUser, user);
        }

        [HttpDelete]
        [Route("changestate")]
        public BaseResult ChangeStateUser([FromUri]int id, bool delete)
        {
            User currentUser = new GetClaimType().GetUserWithClaims(User);
            return ChangeStateUserOperation.Execute(id, currentUser, delete);
        }


    }
}