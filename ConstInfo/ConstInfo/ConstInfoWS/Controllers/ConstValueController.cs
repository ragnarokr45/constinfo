﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations.ConstValueOperation;
using ConstInfoWS.Result;
using ConstInfoWS.Authorize;
using ConstInfoWS.Models;
using System.Security.Claims;
using ConstInfoWS.Providers;
using ConstInfoWS.Claims;

namespace ConstInfoWS.Controllers
{
    [CustomAuthorize]
    [RoutePrefix("constvalue")]
    public class ConstValueController : ApiController
    {
        [HttpGet]
        [Route("values")]
        public GetResult<List<ConstValue>> GetConstValues([FromUri]int id, [FromUri] bool showDeleted = false)
        {
            return GetConstValueOperation.Execute(id, showDeleted);
        }

        [HttpGet]
        [Route("onevalue")]
        public BaseResult GetOneConstValue([FromUri] int id)
        {
            return GetOneConstValueOperation.Execute(id);
        }

        [HttpPost]
        [Route("add")]
        public BaseResult AddConstValues([FromBody]ConstValue constValue)
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return AddConstValueOperation.Execute(user, constValue);
        }

        [HttpPut]
        [Route("update")]
        public BaseResult UpdateConstValues([FromBody]ConstValue constValue)
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return UpdateConstValueOperation.Execute(user, constValue);
        }

        [HttpDelete]
        [Route("changestate")]
        public BaseResult DeleteConstValues([FromUri] int id, bool delete)
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return ChangeStateConstValueOperation.Execute(user, id, delete);
        }

        [HttpPost]
        [Route("canupdate")]
        public BaseResult CanUpdateConstValue([FromBody] ConstValue constValue)
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return CanUpdateConstValueOperation.Execute(user, constValue);
        }
    }
} 

