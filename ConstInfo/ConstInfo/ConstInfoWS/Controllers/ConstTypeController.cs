﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Operations.AccountOperations;
using ConstInfoWS.Result;
using ConstInfoWS.Operations.ConstTypeOperation;
using ConstInfoWS.Authorize;
using ConstInfoWS.Models;
using System.Security.Claims;
using ConstInfoWS.Claims;

namespace ConstInfoWS.Controllers
{
    [RoutePrefix("consttype")]
    public class ConstTypeController : ApiController
    {
        [CustomAuthorize]
        [HttpGet]
        [Route("types")]
        public GetResult<List<ConstType>> GetConstType(bool showDeleted)
        {
            return GetConstTypeOperation.Execute(showDeleted);
        }

        [CustomAuthorize]
        [HttpGet]
        [Route("onetype")]
        public BaseResult GetOneContType([FromUri] int id)
        {
            return GetOneConstTypeOperation.Execute(id);
        }

        [CustomAuthorize(Roles = "admin")]
        [HttpPost]
        [Route("add")]
        public BaseResult AddConstType([FromBody] ConstType constType )
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return AddConstTypeOperation.Execute(user, constType);
        }

        [CustomAuthorize(Roles = "admin")]
        [HttpPut]
        [Route("update")]
        public BaseResult UpdateConstType([FromBody]ConstType constType)
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return UpdateConstTypeOperation.Execute(user, constType);
        }

        [CustomAuthorize(Roles = "admin")]
        [HttpDelete]
        [Route("changestate")]
        public BaseResult ChangeStateConstType([FromUri]int id, bool delete)
        {
            User user = new GetClaimType().GetUserWithClaims(User);
            return ChangeStateConstTypeOperation.Execute(id, user, delete);
        }
    }
}