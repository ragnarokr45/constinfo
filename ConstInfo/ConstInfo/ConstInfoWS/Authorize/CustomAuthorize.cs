﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Security.Principal;
using ConstInfoWS.Result;
using System.Security.Claims;
using ConstInfoWS.Providers;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Validators;
using NLog;

namespace ConstInfoWS.Authorize
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly string[] _emptyArray = new string[0];
        private string[] _rolesSplit;
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {

            _rolesSplit = SplitString(Roles);
            IPrincipal user = actionContext.ControllerContext.RequestContext.Principal;
            if (user == null || user.Identity == null || !user.Identity.IsAuthenticated)
            {
                actionContext.Response = actionContext.ControllerContext.Request.CreateResponse(new BaseResult() { ErrorCode = ErrorCode.BearerError, ErrorDescription = "Invalid bearer token!" });
            }

            if (_rolesSplit.Length > 0 && !_rolesSplit.Any(user.IsInRole))
            {
                actionContext.Response = actionContext.ControllerContext.Request.CreateResponse(new BaseResult() { ErrorCode = ErrorCode.RoleError, ErrorDescription = "Low role!" });
            }
        }


        public override void OnAuthorization(HttpActionContext actionContext)
        {
            IPrincipal User = actionContext.ControllerContext.RequestContext.Principal;
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst("UID");
            int uid = int.TryParse(claim?.Value ?? string.Empty, out uid) ? int.Parse(claim.Value) : 0;
            BaseResult result;
            if (uid == 0) actionContext.Response = actionContext.ControllerContext.Request.CreateResponse(result = new BaseResult() { ErrorCode = ErrorCode.BearerError, ErrorDescription = "Invalid bearer token!" });
            else
            {
                using (DBConnection db = new DBConnection())
                {
                    var validator = new CheckIsUserDeleted(uid, db);
                     result = validator.Validate();
                    if (result.ErrorCode != ErrorCode.Success)
                    {
                        actionContext.Response = actionContext.ControllerContext.Request.CreateResponse(result);
                    }
                }
            }
            logger.Info($"{this.GetType()}: User with id {uid} tried to authorize with result : {result.ErrorDescription}");
        }

        private string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return _emptyArray;
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }
    }
}