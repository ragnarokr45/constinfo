﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Result
{
    public class BaseResult
    {
        public ErrorCode ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
}