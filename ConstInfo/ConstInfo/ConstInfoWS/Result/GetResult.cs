﻿using ConstInfoWS.DB_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConstInfoWS.Result;


namespace ConstInfoWS.Result
{
    public class GetResult<T> : BaseResult  
    {
        public T Result { get; set; }
    }
}       