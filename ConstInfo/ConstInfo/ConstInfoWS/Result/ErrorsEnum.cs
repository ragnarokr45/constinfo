﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConstInfoWS.Result
{
    public enum ErrorCode
    {
        Success = 0,
        CustomError = 1,
        AuthenticationError = 2,
        OwnerError = 3,
        InputError = 4,
        BearerError = 5,
        RoleError = 6,
        UserExistenceError = 7,
        LoginError = 8

    }
}