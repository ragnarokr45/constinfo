﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using ConstInfoWS.Operations.AccountOperations;
using ConstInfoWS.DB_Layer;
using ConstInfoWS.Result;
using System.Net;
using System.Web.Http;
using System.Net.Http;
using Newtonsoft.Json;
using ConstInfoWS.Claims;
using NLog;

namespace ConstInfoWS.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            using (DBConnection db = new DBConnection())
            {
                User user = db.UserLogin(context.UserName, context.Password);

                if (user.Login == null || user.IsDeleted)
                {
                    context.SetError(((int)ErrorCode.AuthenticationError).ToString(), "The user login or password is incorrect.");
                    logger.Info($"{this.GetType()}: someone tried to autorize with result: {context.ErrorDescription}");
                    return;
                }
                ClaimsIdentity identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(CustomClaimTypes.sub, user.Login));
                identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.ToString()));
                identity.AddClaim(new Claim(CustomClaimTypes.UID, user.Id.ToString()));
                identity.AddClaim(new Claim(CustomClaimTypes.isDeleted, user.IsDeleted.ToString()));
                AuthenticationProperties properties = CreateProperties(user);
                AuthenticationTicket ticket = new AuthenticationTicket(identity, properties);
                context.Validated(ticket);
                db.Dispose();
                
            }
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(User user)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "id", user.Id.ToString() },
                { "login", user.Login },
                { "role", ((int)user.Role).ToString() },
                { "firstName", user.FirstName },
                { "secondName", user.SecondName }

            };
            return new AuthenticationProperties(data);
        }


    }
}