﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace ConstInfo.Serializers
{
    public static class JsonSerialDeserial
    {
        public static string Serialize<T>(this T obj)
        {
            string returnVal = "";
            try
            {
                var serializer = new DataContractJsonSerializer(obj.GetType());
                using (var ms = new MemoryStream())
                {
                    serializer.WriteObject(ms, obj);
                    returnVal = Encoding.UTF8.GetString(ms.ToArray());
                }
            }
            catch (Exception ex)
            {
                returnVal = "";
            }
            return returnVal;
        }

        public static T Deserialize<T>(this string json)
        {
            var instance = Activator.CreateInstance<T>();
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serializer = new DataContractJsonSerializer(instance.GetType());
                instance = (T)serializer.ReadObject(ms);
            }
            return instance;
        }
    }
}
