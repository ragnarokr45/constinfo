﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Serializers;

namespace ConstInfo
{
    public static class RestClient
    {

        public static T ActObj<T>(string url, RestMethods method, string post_data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            string result;
            request.Method = method.ToString();
            request.ContentType = "application/json; charset=utf-8";
            request.Accept = "application/json; charset=utf-8";
            request.Headers["Authorization"] = "Bearer " + Storage.Instance.Token?.AccessToken; 
            if (post_data != null)
            {
                using (var streamWriter = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    streamWriter.Write(post_data);
                    streamWriter.Flush();
                }
            }
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            T res = JsonSerialDeserial.Deserialize<T>(result);
            return res;

        }
        public static T GetObj<T>(string url)
        {
            return ActObj<T>(url, RestMethods.Get, null);
        }
        public static T PostObj<T>(string url, T data)
        {
            string dataS = JsonSerialDeserial.Serialize<T>(data);
            return ActObj<T>(url, RestMethods.Post, dataS);
        }
        public static void PutObj<T>(string url, T upDate)
        {
            string dataS = JsonSerialDeserial.Serialize<T>(upDate);
            T res = ActObj<T>(url, RestMethods.Put, dataS);
        }
        public static void DeleteObj<T>(string url, T data)
        {
            string dataS = JsonSerialDeserial.Serialize<T>(data);
            T res = ActObj<T>(url, RestMethods.Delete, dataS);
        }
      
    }
}
