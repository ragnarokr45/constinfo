﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result.RsultForForm.BranchResults;

namespace ConstInfo
{
    public partial class AddBranchForm : Form
    {
        public Branch branch;
        public AddBranchForm()
        {
            InitializeComponent();
        }

        private void AddBranch()
        {
            branch = PostBranchResult.Execute(nameTextBox.Text);
            if (branch != null)
            {
                this.Close();
            }
        }

        private void addBranchButton_Click(object sender, EventArgs e)
        {
            AddBranch();
        }
    }
}
