﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace ConstInfo.Cryptography
{
    class MD5Hash : ICrypter
    {
        string _data;
        public MD5Hash(string data)
        {
            _data = data;
        }
        public string Crypt()
        {
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] bData = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(_data));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < bData.Length; i++)
                {
                    sBuilder.Append(bData[i].ToString("x2"));
                }
                return sBuilder.ToString();
            }
        }
    }
}
