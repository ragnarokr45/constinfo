﻿namespace ConstInfo
{
    partial class AddConstValueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.valueTextBox = new System.Windows.Forms.TextBox();
            this.remarkTextBox = new System.Windows.Forms.TextBox();
            this.addConstValueButton = new System.Windows.Forms.Button();
            this.usersComboBox = new System.Windows.Forms.ComboBox();
            this.branchComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.userLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nameTextBox.Location = new System.Drawing.Point(84, 20);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(180, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // valueTextBox
            // 
            this.valueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.valueTextBox.Location = new System.Drawing.Point(84, 46);
            this.valueTextBox.Name = "valueTextBox";
            this.valueTextBox.Size = new System.Drawing.Size(180, 20);
            this.valueTextBox.TabIndex = 1;
            // 
            // remarkTextBox
            // 
            this.remarkTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.remarkTextBox.Location = new System.Drawing.Point(84, 99);
            this.remarkTextBox.Name = "remarkTextBox";
            this.remarkTextBox.Size = new System.Drawing.Size(180, 20);
            this.remarkTextBox.TabIndex = 3;
            // 
            // addConstValueButton
            // 
            this.addConstValueButton.BackColor = System.Drawing.SystemColors.Control;
            this.addConstValueButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addConstValueButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.addConstValueButton.ForeColor = System.Drawing.Color.Black;
            this.addConstValueButton.Location = new System.Drawing.Point(84, 159);
            this.addConstValueButton.Name = "addConstValueButton";
            this.addConstValueButton.Size = new System.Drawing.Size(180, 23);
            this.addConstValueButton.TabIndex = 5;
            this.addConstValueButton.Text = "Add value";
            this.addConstValueButton.UseVisualStyleBackColor = false;
            this.addConstValueButton.Click += new System.EventHandler(this.addConstValueButton_Click);
            // 
            // usersComboBox
            // 
            this.usersComboBox.BackColor = System.Drawing.SystemColors.Control;
            this.usersComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.usersComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.usersComboBox.ForeColor = System.Drawing.Color.Black;
            this.usersComboBox.FormattingEnabled = true;
            this.usersComboBox.Location = new System.Drawing.Point(84, 125);
            this.usersComboBox.Name = "usersComboBox";
            this.usersComboBox.Size = new System.Drawing.Size(180, 21);
            this.usersComboBox.TabIndex = 4;
            this.usersComboBox.Visible = false;
            // 
            // branchComboBox
            // 
            this.branchComboBox.BackColor = System.Drawing.SystemColors.Control;
            this.branchComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.branchComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.branchComboBox.ForeColor = System.Drawing.Color.Black;
            this.branchComboBox.FormattingEnabled = true;
            this.branchComboBox.Location = new System.Drawing.Point(84, 72);
            this.branchComboBox.Name = "branchComboBox";
            this.branchComboBox.Size = new System.Drawing.Size(180, 21);
            this.branchComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Value";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label3.Location = new System.Drawing.Point(12, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Branch";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label4.Location = new System.Drawing.Point(12, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Remark";
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.userLabel.Location = new System.Drawing.Point(12, 128);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(29, 13);
            this.userLabel.TabIndex = 12;
            this.userLabel.Text = "User";
            this.userLabel.Visible = false;
            // 
            // AddConstValueForm
            // 
            this.AcceptButton = this.addConstValueButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(294, 208);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.branchComboBox);
            this.Controls.Add(this.usersComboBox);
            this.Controls.Add(this.addConstValueButton);
            this.Controls.Add(this.remarkTextBox);
            this.Controls.Add(this.valueTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(310, 247);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(310, 247);
            this.Name = "AddConstValueForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add const value";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.TextBox valueTextBox;
        private System.Windows.Forms.TextBox remarkTextBox;
        private System.Windows.Forms.Button addConstValueButton;
        private System.Windows.Forms.ComboBox usersComboBox;
        private System.Windows.Forms.ComboBox branchComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label userLabel;
    }
}