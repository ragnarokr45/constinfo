﻿using ConstInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;

namespace ConstInfo
{
    public sealed class Storage
    {
        private static Storage _instance = null;
        private static readonly object padlock = new object();

        Storage()
        {
        }

        public static Storage Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new Storage();
                        }                    
                    }
                }
                return _instance;
            }
        }
        public User User { get; set; }
        public Token Token { get; set; }
        public bool ShowDeletedValues { get; set; }
        public bool ShowDeletedUsers { get; set; }
        public bool ShowDeletedBranches { get; set; }
        public Color redColor = Color.FromArgb(255, 221, 221);
        public Color greenColor = Color.FromArgb(221, 255, 221);

    }
}
