﻿namespace ConstInfo
{
    partial class AddBranchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.addBranchButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nameTextBox.Location = new System.Drawing.Point(60, 20);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(115, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // addBranchButton
            // 
            this.addBranchButton.BackColor = System.Drawing.SystemColors.Control;
            this.addBranchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addBranchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.addBranchButton.ForeColor = System.Drawing.Color.Black;
            this.addBranchButton.Location = new System.Drawing.Point(60, 50);
            this.addBranchButton.Name = "addBranchButton";
            this.addBranchButton.Size = new System.Drawing.Size(115, 22);
            this.addBranchButton.TabIndex = 1;
            this.addBranchButton.Text = "Add branch";
            this.addBranchButton.UseVisualStyleBackColor = false;
            this.addBranchButton.Click += new System.EventHandler(this.addBranchButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // AddBranchForm
            // 
            this.AcceptButton = this.addBranchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(206, 95);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addBranchButton);
            this.Controls.Add(this.nameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(222, 134);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(222, 134);
            this.Name = "AddBranchForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add branch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button addBranchButton;
        private System.Windows.Forms.Label label1;
    }
}