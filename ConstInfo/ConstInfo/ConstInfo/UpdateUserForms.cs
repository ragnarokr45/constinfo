﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;
using System.Runtime.InteropServices;
using ConstInfo.Result.RsultForForm.UserResults;
using ConstInfo.Alerts;

namespace ConstInfo
{
    public partial class UpdateUserForms : Form
    {
        User _user;
        int _id;
        public UpdateUserForms(int id)
        {
            InitializeComponent();
            _id = id;
            GetData();
        }

        private void GetData()
        {
            _user = GetUserByIdResult.Execute(_id);
            FillForm();
        }

        private void FillForm()
        {
            if (_user != null)
            {
                loginListBox.Text = _user.Login;
                firstNameTextBox.Text = _user.FirstName;
                secondNameTextBox.Text = _user.SecondName;
                roleComboBox.DataSource = Enum.GetNames(typeof(Roles));
                roleComboBox.SelectedItem = _user.Role.ToString();
            }
        }

        private void UpdateUser()
        {
            
            if (PutUserResult.Execute(
                _user.Id, 
                loginListBox.Text, 
                firstNameTextBox.Text, 
                secondNameTextBox.Text, 
                passwordTextBox.Text, 
                (Roles)Enum.Parse(typeof(Roles), roleComboBox.Text)))
            {
                this.Close();
            }
        }

        private void updateUserButton_Click(object sender, EventArgs e)
        {
            if (MessageBoxAlerts.SureMessage())
            {
                UpdateUser();
            }
        }
    }
}
