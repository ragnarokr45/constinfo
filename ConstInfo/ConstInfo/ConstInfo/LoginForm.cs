﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.DataReceiver;
using ConstInfo.Alerts;
using ConstInfo.Alerts.Errors;
using ConstInfo.Cryptography;


namespace ConstInfo
{
    public partial class LoginForm : Form
    {      
        public LoginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginHttpRequest.Execute(Properties.Settings.Default["BaseUrl"] + "/token", RestMethods.Post, 
                $"grant_type=password&username={textBox1.Text}&password={new MD5Hash(textBox2.Text).Crypt()}");
            if (Storage.Instance.User != null)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.AuthenticationError);
            }
        }
    }
}
