﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Models
{
    [DataContract]
    public class ConstValue
    {
        [DataMember(Name = "valueId")]
        public int ValueId { get; set; }

        [DataMember(Name = "user")]
        public User User { get; set; }

        [DataMember(Name = "constType")]
        public ConstType ConstType { get; set; }

        [DataMember(Name = "date")]
        private string _date { get; set; }
        public DateTime Date { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }

        [DataMember(Name = "branch")]
        public Branch Branch { get; set; }

        [DataMember(Name = "remark")]
        public string Remark { get; set; }

        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }


        [OnSerializing]
        public void Serializing(StreamingContext context)
        {
            _date = String.Format("{0:s}", Date);
        }

        [OnDeserialized]
        public void Deserializing(StreamingContext context)
        {
            Date = Convert.ToDateTime(_date);
        }
    }
}
