﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Models
{
    [DataContract]
    public class User
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "login")]
        public string Login { get; set; }
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }
        [DataMember(Name = "secondName")]
        public string SecondName { get; set; }
        [DataMember(Name = "password")]
        public string Password { get; set; }
        [DataMember(Name = "role")]
        public Roles Role { get; set; }
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }
    }
    public enum Roles
    {
        admin = 1,
        user
    }
}
