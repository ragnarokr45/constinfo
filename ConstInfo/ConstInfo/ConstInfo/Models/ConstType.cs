﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Models
{
    
    [DataContract]
    public class ConstType
    {

        [DataMember(Name = "id")]
        public int Id { get; set; }
        [DataMember(Name = "constName")]
        public string ConstName { get; set; }
        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }
    }
}
