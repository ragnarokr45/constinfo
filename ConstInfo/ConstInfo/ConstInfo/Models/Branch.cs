﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace ConstInfo.Models
{
    [DataContract]
    public class Branch
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "branchName")]
        public string BranchName { get; set; }

        [DataMember(Name = "isDeleted")]
        public bool IsDeleted { get; set; }
    }
}
