﻿namespace ConstInfo
{
    partial class UsersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsersForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.addUserToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.updateUserToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteUserToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.restoreToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.showDeletedToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.usersDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usersContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userRestoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).BeginInit();
            this.usersContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(768, 24);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addUserToolStripButton,
            this.updateUserToolStripButton,
            this.deleteUserToolStripButton,
            this.restoreToolStripButton,
            this.refreshToolStripButton,
            this.showDeletedToolStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(768, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // addUserToolStripButton
            // 
            this.addUserToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addUserToolStripButton.Image = global::ConstInfo.Properties.Resources.add;
            this.addUserToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addUserToolStripButton.Name = "addUserToolStripButton";
            this.addUserToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.addUserToolStripButton.Text = "Add";
            this.addUserToolStripButton.Click += new System.EventHandler(this.addUserToolStripButton_Click);
            // 
            // updateUserToolStripButton
            // 
            this.updateUserToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.updateUserToolStripButton.Image = global::ConstInfo.Properties.Resources.edit;
            this.updateUserToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.updateUserToolStripButton.Name = "updateUserToolStripButton";
            this.updateUserToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.updateUserToolStripButton.Text = "Edit";
            this.updateUserToolStripButton.Click += new System.EventHandler(this.updateUserToolStripButton_Click);
            // 
            // deleteUserToolStripButton
            // 
            this.deleteUserToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteUserToolStripButton.Image = global::ConstInfo.Properties.Resources.delete;
            this.deleteUserToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteUserToolStripButton.Name = "deleteUserToolStripButton";
            this.deleteUserToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteUserToolStripButton.Text = "delete";
            this.deleteUserToolStripButton.Click += new System.EventHandler(this.deleteUserToolStripButton_Click);
            // 
            // restoreToolStripButton
            // 
            this.restoreToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.restoreToolStripButton.Image = global::ConstInfo.Properties.Resources.new_rel;
            this.restoreToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.restoreToolStripButton.Name = "restoreToolStripButton";
            this.restoreToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.restoreToolStripButton.Text = "toolStripButton1";
            this.restoreToolStripButton.Click += new System.EventHandler(this.restoreToolStripButton_Click);
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshToolStripButton.Image = global::ConstInfo.Properties.Resources.refresh;
            this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.refreshToolStripButton.Text = "Refresh";
            this.refreshToolStripButton.Click += new System.EventHandler(this.refreshToolStripButton_Click);
            // 
            // showDeletedToolStripButton
            // 
            this.showDeletedToolStripButton.BackColor = System.Drawing.Color.White;
            this.showDeletedToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.showDeletedToolStripButton.Image = global::ConstInfo.Properties.Resources.view;
            this.showDeletedToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.showDeletedToolStripButton.Name = "showDeletedToolStripButton";
            this.showDeletedToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.showDeletedToolStripButton.Text = "Show deleted";
            this.showDeletedToolStripButton.Click += new System.EventHandler(this.showDeletedToolStripButton_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.usersDataGridView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(768, 396);
            this.panel2.TabIndex = 1;
            // 
            // usersDataGridView
            // 
            this.usersDataGridView.AllowUserToAddRows = false;
            this.usersDataGridView.AllowUserToDeleteRows = false;
            this.usersDataGridView.AllowUserToResizeColumns = false;
            this.usersDataGridView.AllowUserToResizeRows = false;
            this.usersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.usersDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.usersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usersDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.usersDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usersDataGridView.Location = new System.Drawing.Point(0, 0);
            this.usersDataGridView.MultiSelect = false;
            this.usersDataGridView.Name = "usersDataGridView";
            this.usersDataGridView.ReadOnly = true;
            this.usersDataGridView.Size = new System.Drawing.Size(768, 396);
            this.usersDataGridView.TabIndex = 0;
            this.usersDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.usersDataGridView_CellMouseClick);
            this.usersDataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.usersDataGridView_RowHeaderMouseClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Login";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "FirstName";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "SecondName";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Role";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // usersContextMenuStrip
            // 
            this.usersContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addUserToolStripMenuItem,
            this.deleteUserToolStripMenuItem,
            this.updateUserToolStripMenuItem,
            this.userRestoreToolStripMenuItem});
            this.usersContextMenuStrip.Name = "usersContextMenuStrip";
            this.usersContextMenuStrip.Size = new System.Drawing.Size(114, 92);
            // 
            // addUserToolStripMenuItem
            // 
            this.addUserToolStripMenuItem.Name = "addUserToolStripMenuItem";
            this.addUserToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.addUserToolStripMenuItem.Text = "Add";
            this.addUserToolStripMenuItem.Click += new System.EventHandler(this.addUserToolStripMenuItem_Click);
            // 
            // deleteUserToolStripMenuItem
            // 
            this.deleteUserToolStripMenuItem.Name = "deleteUserToolStripMenuItem";
            this.deleteUserToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.deleteUserToolStripMenuItem.Text = "Delete";
            this.deleteUserToolStripMenuItem.Click += new System.EventHandler(this.deleteUserToolStripMenuItem_Click);
            // 
            // updateUserToolStripMenuItem
            // 
            this.updateUserToolStripMenuItem.Name = "updateUserToolStripMenuItem";
            this.updateUserToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.updateUserToolStripMenuItem.Text = "Update";
            this.updateUserToolStripMenuItem.Click += new System.EventHandler(this.updateUserToolStripMenuItem_Click);
            // 
            // userRestoreToolStripMenuItem
            // 
            this.userRestoreToolStripMenuItem.Name = "userRestoreToolStripMenuItem";
            this.userRestoreToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.userRestoreToolStripMenuItem.Text = "Restore";
            this.userRestoreToolStripMenuItem.Click += new System.EventHandler(this.userRestoreToolStripMenuItem_Click);
            // 
            // UsersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 420);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UsersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Users";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UsersForm_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).EndInit();
            this.usersContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView usersDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.ToolStripButton addUserToolStripButton;
        private System.Windows.Forms.ToolStripButton updateUserToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteUserToolStripButton;
        private System.Windows.Forms.ToolStripButton refreshToolStripButton;
        private System.Windows.Forms.ToolStripButton showDeletedToolStripButton;
        private System.Windows.Forms.ContextMenuStrip usersContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem userRestoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton restoreToolStripButton;
    }
}