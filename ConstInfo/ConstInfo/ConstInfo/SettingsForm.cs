﻿using ConstInfo.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConstInfo
{
    public partial class SettingsForm : Form
    {
        private bool dirtyData;
        public SettingsForm()
        {
            InitializeComponent();
            AdminMenu();
            FillForm();
        }

        private void AdminMenu()
        {
            var adminValidate = new CheckAdmin();
            if (adminValidate.Validate())
            {
                baseUrlTextBox.ReadOnly = false;
            }
        }

        private void FillForm()
        {
            baseUrlTextBox.Text = Properties.Settings.Default["BaseUrl"].ToString();
            dirtyData = false;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveSettings();
            this.Close();
        }

        private void SaveSettings()
        {
            Properties.Settings.Default["BaseUrl"] = baseUrlTextBox.Text;
            dirtyData = false;
        }

        private void AskAboutUpdate()
        {
            if (dirtyData)
            {
                if (MessageBox.Show("Save changes", "Settings", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SaveSettings();
                }
                else
                {
                    dirtyData = false;
                }
            }
        }

        private void baseUrlTextBox_TextChanged(object sender, EventArgs e)
        {
            dirtyData = true;
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            AskAboutUpdate();
            e.Cancel = false;
        }
    }
}
