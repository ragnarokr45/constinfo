﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result.RsultForForm.BranchResults;
using ConstInfo.Alerts;
using ConstInfo.Alerts.Errors;
using ConstInfo.Validators;

namespace ConstInfo
{
    public partial class BranchesForm : Form
    {
        List<Branch> _branches;
        Storage store = Storage.Instance;
        bool _adminAuthority = false;
        int _selectedIdBranch = 0;
        public BranchesForm()
        {
            InitializeComponent();
            store.ShowDeletedBranches = false;
            AdminCheck();
            ListSettings();
            GetData();
        }
        
        private void AdminCheck()
        {
            var checkAdmin = new CheckAdmin();
            if (checkAdmin.Validate())
            {
                _adminAuthority = true;
            }
        }

        private void ListSettings()
        {
            showDelToolStripButton.BackColor = store.redColor;
            branchesListView.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private void GetData()
        {
            _branches = new List<Branch>();
            _branches = GetBranchResult.Execute();
            if (_branches != null)
            {
                ShowBranches();
            }
        }

        private void ShowBranches()
        {
            branchesListView.Items.Clear();
            _branches = _branches.OrderBy(x => x.BranchName).ToList();
            for (int i = 0; i < _branches.Count; i++)
            {
                branchesListView.Items.Add(CreateListViewItem(_branches[i]));
                if (_branches[i].IsDeleted)
                {
                    branchesListView.Items[branchesListView.Items.Count - 1].BackColor = store.redColor;
                }
            }
            var ct = branchesListView.Items.Cast<ListViewItem>().ToList().Find(x => (int)x.Tag == _selectedIdBranch);
            if (ct != null)
            {
                ct.Selected = true;
            }
            else
            {
                _selectedIdBranch = 0;
                var listItem = branchesListView.Items.Cast<ListViewItem>().ToList().FirstOrDefault();
                if (listItem != null)
                {
                    listItem.Selected = true;
                }
            }
        }

        private ListViewItem CreateListViewItem(Branch branch)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = branch.BranchName;
            lvi.Tag = branch.Id;
            return lvi;
        }

        private void DeleteBranch()
        {
            Branch b = _branches.Find(c => c.Id == (int)branchesListView.SelectedItems[0].Tag);
            if (!b.IsDeleted)
            {
                if (MessageBoxAlerts.SureMessage())
                {
                    DeleteBranchResult.Execute(b.Id, true);
                    GetData();
                }
            }
        }

        private void UpdateBranch()
        {
            if (branchesListView.SelectedItems.Count != 0)
            {
                UpdateBranchForm updateBranchForm = new UpdateBranchForm((int)branchesListView.SelectedItems[0].Tag);
                updateBranchForm.ShowDialog();
                GetData();
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void AddBranch()
        {
            AddBranchForm addBranchForm = new AddBranchForm();
            addBranchForm.ShowDialog();
            if (addBranchForm.branch != null)
            {
                _selectedIdBranch = addBranchForm.branch.Id;
            }
            GetData();
        }

        private void Restore()
        {
            Branch branch = _branches.Find(x => x.BranchName == branchesListView.FocusedItem.Text);
            DeleteBranchResult.Execute(branch.Id, false);
            GetData();
        }

        private void addToolStripButton_Click(object sender, EventArgs e)
        {
            AddBranch();
        }

        private void updateToolStripButton_Click(object sender, EventArgs e)
        {
            UpdateBranch();
        }

        private void deleteToolStripButton_Click(object sender, EventArgs e)
        {
            if (branchesListView.SelectedItems.Count != 0)
            {
                DeleteBranch();
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            GetData();
        }

        private void showDelToolStripButton_Click(object sender, EventArgs e)
        {
            store.ShowDeletedBranches = !store.ShowDeletedBranches;
            if (store.ShowDeletedBranches) showDelToolStripButton.BackColor = store.greenColor;
            else showDelToolStripButton.BackColor = store.redColor;
            GetData();
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Restore();
        }

        private void branchesListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (branchesListView.FocusedItem.Bounds.Contains(e.Location))
                {
                    if (_adminAuthority && _branches.Find(x => x.BranchName == branchesListView.FocusedItem.Text).IsDeleted)
                        restoreToolStripMenuItem.Enabled = true;
                    else
                        restoreToolStripMenuItem.Enabled = false;
                    branchesContextMenuStrip.Show(Cursor.Position);
                }
            }
        }

        private void branchesListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (branchesListView.SelectedItems.Count == 0)
                return;
            _selectedIdBranch = (int)branchesListView.SelectedItems[0].Tag;
        }

        private void addBranchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddBranch();
        }

        private void updateBranchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateBranch();
        }

        private void deleteBranchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteBranch();
        }

        private void restoreToolStripButton1_Click(object sender, EventArgs e)
        {
            Restore();
        }
    }
}
