﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result.RsultForForm.BranchResults;
using ConstInfo.Result;
using ConstInfo.Alerts;

namespace ConstInfo
{
    public partial class UpdateBranchForm : Form
    {
        Branch _branch;
        int _id;
        public UpdateBranchForm(int id)
        {
            InitializeComponent();
            _id = id;
            GetData();
        }

        private void GetData()
        {
            _branch = GetBranchByIdResult.Execute(_id);
            FillForm();
        }

        private void FillForm()
        {
            if (_branch != null)
            {
                nameTextBox.Text = _branch.BranchName;
            }
        }

        private void UpdateBranch()
        {
            if (PutBranchResult.Execute(_branch.Id, nameTextBox.Text))
            {
                this.Close();
            }
        }

        private void updateBranchButton_Click(object sender, EventArgs e)
        {
            if (MessageBoxAlerts.SureMessage())
            {
                UpdateBranch();
            }
        }
    }
}
