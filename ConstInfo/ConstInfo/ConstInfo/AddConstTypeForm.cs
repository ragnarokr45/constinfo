﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;
using ConstInfo.Result.RsultForForm.TypesResults;

namespace ConstInfo
{
    public partial class AddConstTypeForm : Form
    {
        public ConstType constType;
        private string _baseUrl = String.Empty;
        public AddConstTypeForm()
        {
            InitializeComponent();
            _baseUrl = Properties.Settings.Default["BaseUrl"].ToString();
        }

        private void AddConstType()
        {
            constType = PostConstTypeResult.Execute(nameTextBox.Text);
            if (constType != null)
            {
                this.Close();
            }
        }

        private void addConstTypeButton_Click(object sender, EventArgs e)
        {
            AddConstType();
        }
    }
}
