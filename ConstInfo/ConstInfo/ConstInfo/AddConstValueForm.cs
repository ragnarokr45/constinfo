﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.DataReceiver;
using ConstInfo.Result;
using ConstInfo.Models;
using ConstInfo.Result.RsultForForm.ValuesResults;
using ConstInfo.Result.RsultForForm.UserResults;
using ConstInfo.Result.RsultForForm.BranchResults;
using ConstInfo.Validators;

namespace ConstInfo
{
    public partial class AddConstValueForm : Form
    {
        public ConstValue constValue;
        ConstType _constType;
        List<User> _users;
        List<Branch> _branches;
        public AddConstValueForm(ConstType constType)
        {        
            InitializeComponent();
            _constType = constType;
            GetBranches();
            AdminPower();
        }

        private void GetBranches()
        {
            _branches = GetBranchResult.Execute(false);
            if (_branches != null)
            {
                _branches = _branches.OrderBy(x => x.BranchName).ToList();
                foreach (var item in _branches)
                {
                    ComboboxItem cb = new ComboboxItem();
                    cb.Text = item.BranchName;
                    cb.Value = item.Id;
                    branchComboBox.Items.Add(cb);
                }
                branchComboBox.SelectedIndex = 0;
            }
        }

        private void AdminPower()
        {
            var adminCheck = new CheckAdmin();
            if (adminCheck.Validate())
            {
                _users = GetUserResult.Execute(false);
                if (_users != null)
                {
                    _users = _users.OrderBy(x => x.SecondName).ThenBy(x => x.FirstName).ToList();
                    foreach (var item in _users)
                    {
                        ComboboxItem cb = new ComboboxItem();
                        cb.Text = item.SecondName == "" && item.FirstName == "" ? item.Login : $"{item.SecondName} {item.FirstName}";
                        cb.Value = item.Id;
                        usersComboBox.Items.Add(cb);
                    }
                    User admin = Storage.Instance.User;
                    string findStr = admin.SecondName == "" && admin.FirstName == "" ?
                        admin.Login :
                        $"{admin.SecondName} {admin.FirstName}";
                    usersComboBox.SelectedIndex = usersComboBox.FindStringExact(findStr);
                }
                usersComboBox.Visible = true;
                userLabel.Visible = true;
            }
        }

        private void AddConstValue()
        {
            constValue = PostConstValueResult.Execute(
                nameTextBox.Text,
                valueTextBox.Text,
                _branches.Find(x => x.Id == (branchComboBox.SelectedItem as ComboboxItem).Value),
                remarkTextBox.Text,
                _constType,
                _users.Find(x => x.Id == (usersComboBox.SelectedItem as ComboboxItem).Value)
                );
            if (constValue != null)
            {
                this.Close();
            }
        }

        private void addConstValueButton_Click(object sender, EventArgs e)
        {
            AddConstValue();
        }
    }
}
