﻿namespace ConstInfo
{
    partial class UpdateConstTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.updateConstTypeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(56, 19);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(123, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // updateConstTypeButton
            // 
            this.updateConstTypeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateConstTypeButton.Location = new System.Drawing.Point(56, 53);
            this.updateConstTypeButton.Name = "updateConstTypeButton";
            this.updateConstTypeButton.Size = new System.Drawing.Size(123, 23);
            this.updateConstTypeButton.TabIndex = 1;
            this.updateConstTypeButton.Text = "Update const type";
            this.updateConstTypeButton.UseVisualStyleBackColor = true;
            this.updateConstTypeButton.Click += new System.EventHandler(this.updateConstTypeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // UpdateConstTypeForm
            // 
            this.AcceptButton = this.updateConstTypeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(212, 92);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.updateConstTypeButton);
            this.Controls.Add(this.nameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(228, 131);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(228, 131);
            this.Name = "UpdateConstTypeForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update const type";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button updateConstTypeButton;
        private System.Windows.Forms.Label label1;
    }
}