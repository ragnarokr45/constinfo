﻿namespace ConstInfo
{
    partial class AddConstTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.addConstTypeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.nameTextBox.Location = new System.Drawing.Point(56, 19);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(123, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // addConstTypeButton
            // 
            this.addConstTypeButton.BackColor = System.Drawing.SystemColors.Control;
            this.addConstTypeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addConstTypeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.addConstTypeButton.ForeColor = System.Drawing.Color.Black;
            this.addConstTypeButton.Location = new System.Drawing.Point(56, 53);
            this.addConstTypeButton.Name = "addConstTypeButton";
            this.addConstTypeButton.Size = new System.Drawing.Size(123, 24);
            this.addConstTypeButton.TabIndex = 1;
            this.addConstTypeButton.Text = "Add const type";
            this.addConstTypeButton.UseVisualStyleBackColor = false;
            this.addConstTypeButton.Click += new System.EventHandler(this.addConstTypeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // AddConstTypeForm
            // 
            this.AcceptButton = this.addConstTypeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(212, 92);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.addConstTypeButton);
            this.Controls.Add(this.nameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(228, 131);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(228, 131);
            this.Name = "AddConstTypeForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add const type";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button addConstTypeButton;
        private System.Windows.Forms.Label label1;
    }
}