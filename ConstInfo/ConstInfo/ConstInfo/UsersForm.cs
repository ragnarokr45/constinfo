﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Models;
using ConstInfo.Result;
using ConstInfo.DataReceiver;
using ConstInfo.Result.RsultForForm.UserResults;
using ConstInfo.Alerts;
using ConstInfo.Alerts.Errors;
using ConstInfo.Validators;

namespace ConstInfo
{
    public partial class UsersForm : Form
    {
        List<User> _users;
        Storage store = Storage.Instance;
        bool _adminAuthority = false;
        int _loginColumn = 0;
        int _selectedIdUser = 0;
        public UsersForm()
        {
            InitializeComponent();
            store.ShowDeletedValues = false;
            FormItemSettings();
            AdminMenu();
            GetUsers();
            AddRows();
        }

        private void AdminMenu()
        {
            var adminValidator = new CheckAdmin();
            if (adminValidator.Validate())
            {
                panel1.Visible = true;
                _adminAuthority = true;
            }
        }

        private void FormItemSettings()
        {
            showDeletedToolStripButton.BackColor = store.redColor;
        }

        private void GetUsers()
        {
            _users = new List<User>();
            _users = GetUserResult.Execute();
            AddRows();
        }

        private User GetUserById()
        {
            int idUser = (int)usersDataGridView.Rows[usersDataGridView.SelectedCells[0].RowIndex].Tag;
            return _users.Find(x => x.Id == idUser);
        }

        private void AddRows()
        {
            usersDataGridView.Rows.Clear();
            if (_users != null)
            {
                foreach (var item in _users)
                {
                    AddRow(item);
                }
                var us = usersDataGridView.Rows.Cast<DataGridViewRow>().ToList().Find(x => (int)x.Tag == _selectedIdUser);
                if (us != null)
                {
                    usersDataGridView.Rows[0].Cells[0].Selected = false;
                    us.Cells[0].Selected = true;
                    us.Selected = true;
                }
                else
                {
                    var itemIdGridView = usersDataGridView.Rows.GetFirstRow(0);
                    if (itemIdGridView != -1)
                    {
                        _selectedIdUser = 0;
                        usersDataGridView.Rows[itemIdGridView].Selected = true;
                        usersDataGridView.Rows[itemIdGridView].Cells[0].Selected = true;
                    }
                }
            }
        }

        private void AddRow(User user)
        {
            DataGridViewRow dgvRow = new DataGridViewRow();
            dgvRow.CreateCells(usersDataGridView);
            dgvRow.Cells[0].Value = user.Login;
            dgvRow.Cells[1].Value = user.FirstName;
            dgvRow.Cells[2].Value = user.SecondName;
            dgvRow.Cells[3].Value = user.Role;
            dgvRow.Tag = user.Id;
            usersDataGridView.Rows.Add(dgvRow);
            if (user.IsDeleted)
            {
                usersDataGridView.Rows[usersDataGridView.Rows.Count - 1].DefaultCellStyle.BackColor = store.redColor;
            }
        }

        private void DeleteUser()
        {
            if (usersDataGridView.SelectedCells.Count != 0)
            {
                User user = GetUserById();
                if (!user.IsDeleted)
                {
                    if (MessageBoxAlerts.SureMessage())
                    {
                        DeleteUserResult.Execute(user.Id, true);
                        GetUsers();
                    }
                }
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void AddUser()
        {
            AddUserForm addUserForm = new AddUserForm();
            addUserForm.ShowDialog();
            if (addUserForm.user != null)
            {
                _selectedIdUser = addUserForm.user.Id;
            }
            GetUsers();
        }

        private void UpdateUser()
        {
            if (usersDataGridView.SelectedCells.Count != 0)
            {
                User user = GetUserById();
                if (!user.IsDeleted)
                {
                    UpdateUserForms updateUserForm = new UpdateUserForms(user.Id);
                    updateUserForm.ShowDialog();
                    GetUsers();
                }
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void RestoreUser()
        {
            if (usersDataGridView.SelectedCells.Count != 0)
            {
                User user = GetUserById();
                DeleteUserResult.Execute(user.Id, false);
                GetUsers();
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void addUserToolStripButton_Click(object sender, EventArgs e)
        {
            AddUser();
        }

        private void updateUserToolStripButton_Click(object sender, EventArgs e)
        {
            UpdateUser();
        }

        private void deleteUserToolStripButton_Click(object sender, EventArgs e)
        {
            DeleteUser();
        }

        private void showDeletedToolStripButton_Click(object sender, EventArgs e)
        {
            store.ShowDeletedUsers = !store.ShowDeletedUsers;
            if (store.ShowDeletedUsers) showDeletedToolStripButton.BackColor = store.greenColor;
            else showDeletedToolStripButton.BackColor = store.redColor;
            GetUsers();
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            GetUsers();
        }

        private void userRestoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestoreUser();
        }

        private void usersDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                _selectedIdUser = (int)usersDataGridView.Rows[e.RowIndex].Tag;
            }
            if (e.Button == MouseButtons.Right && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                usersDataGridView.CurrentCell = usersDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];
                usersDataGridView.Rows[e.RowIndex].Selected = true;
                _selectedIdUser = (int)usersDataGridView.Rows[e.RowIndex].Tag;
                if (usersDataGridView.Bounds.Contains(e.Location))
                {
                    if (_adminAuthority && _users.Find(x => x.Login == usersDataGridView.SelectedRows[0].Cells[_loginColumn].Value.ToString()).IsDeleted)
                        userRestoreToolStripMenuItem.Enabled = true;
                    else
                        userRestoreToolStripMenuItem.Enabled = false;
                    usersContextMenuStrip.Show(Cursor.Position);
                }
            }
        }

        private void usersDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                _selectedIdUser = (int)usersDataGridView.SelectedRows[0].Tag;
            }
        }

        private void addUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddUser();
        }

        private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteUser();
        }

        private void updateUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateUser();
        }

        private void UsersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            store.ShowDeletedUsers = false;
        }

        private void restoreToolStripButton_Click(object sender, EventArgs e)
        {
            RestoreUser();
        }
    }
}
