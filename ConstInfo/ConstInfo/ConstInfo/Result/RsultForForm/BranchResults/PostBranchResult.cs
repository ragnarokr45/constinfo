﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.BranchResults
{
    class PostBranchResult : BaseResultForRorm<PostBranchResult, Branch>
    {
        Branch _branch;
        public PostBranchResult(string name)
        {
            _branch = new Branch() 
            { 
                BranchName = name
            };
        }
        public override Branch GetRequestResult()
        {
            GetResult<Branch> result = RestHttpRequest<GetResult<Branch>, Branch>.Execute(baseUrl + "/branch/add", RestMethods.Post, _branch);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
