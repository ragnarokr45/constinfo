﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.BranchResults
{
    class DeleteBranchResult : BaseResultForRorm<DeleteBranchResult, bool>
    {
        int _id;
        bool _delete;
        public DeleteBranchResult(int id, bool delete)
        {
            _id = id;
            _delete = delete;
        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, string>.Execute(baseUrl + $"/branch/changestate?id={_id}&delete={_delete}", RestMethods.Delete);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
