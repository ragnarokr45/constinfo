﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.BranchResults
{
    class PutBranchResult : BaseResultForRorm<PutBranchResult, bool>
    {
        Branch _branch;
        public PutBranchResult(int id, string name)
        {
            _branch = new Branch() 
            { 
                Id = id,
                BranchName = name
            };

        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, Branch>.Execute(baseUrl + "/branch/update", RestMethods.Put, _branch);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
