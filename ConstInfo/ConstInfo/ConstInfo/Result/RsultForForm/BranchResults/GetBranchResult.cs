﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.BranchResults
{
    class GetBranchResult : BaseResultForRorm<GetBranchResult, List<Branch>>
    {
        public GetBranchResult()
        {

        }
        public GetBranchResult(bool deletedBranches)
        {
            showDeletedBranches = deletedBranches;
        }
        public override List<Branch> GetRequestResult()
        {
            GetResult<List<Branch>> result = RestHttpRequest<GetResult<List<Branch>>, string>.Execute(baseUrl + $"/branch/branches?showDeleted={showDeletedBranches}", RestMethods.Get);
            if (result != null)
            {
                return result.Result;
            }
            return new List<Branch>();
        }
    }
}
