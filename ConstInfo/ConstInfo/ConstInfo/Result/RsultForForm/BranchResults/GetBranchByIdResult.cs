﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.BranchResults
{
    class GetBranchByIdResult : BaseResultForRorm<GetBranchByIdResult, Branch>
    {
        int _id;
        public GetBranchByIdResult(int id)
        {
            _id = id;
        }
        public override Branch GetRequestResult()
        {
            GetResult<Branch> result = RestHttpRequest<GetResult<Branch>, string>.Execute(baseUrl + $"/branch/onebranch?id={_id}", RestMethods.Get);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
