﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;

namespace ConstInfo.Result.RsultForForm.UserResults
{
    class GetUserByIdResult : BaseResultForRorm<GetUserByIdResult, User>
    {
        int _id;
        public GetUserByIdResult(int id)
        {
            _id = id;
        }
        public override User GetRequestResult()
        {
            GetResult<User> result = RestHttpRequest<GetResult<User>, string>.Execute(baseUrl + $"/account/oneuser?id={_id}", RestMethods.Get);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
