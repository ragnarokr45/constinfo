﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.DataReceiver;
using ConstInfo.Models;
using ConstInfo.Cryptography;

namespace ConstInfo.Result.RsultForForm.UserResults
{
    class DeleteUserResult : BaseResultForRorm<DeleteUserResult, bool>
    {
        int _id;
        bool _delete;
        public DeleteUserResult(int id, bool delete)
        {
            _id = id;
            _delete = delete;
        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, ConstType>.Execute(
                Properties.Settings.Default["BaseUrl"].ToString() + $"/account/changestate?id={_id}&delete={_delete}", RestMethods.Delete);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
