﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.DataReceiver;
using ConstInfo.Models;
using ConstInfo.Cryptography;

namespace ConstInfo.Result.RsultForForm.UserResults
{
    class PostUserResult : BaseResultForRorm<PostUserResult, User>
    {
        private User _user;
        public PostUserResult(string login, string firstName, string secondName, string password, Roles role)
        {
            _user = new User()
            {
                Login = login,
                FirstName = firstName,
                SecondName = secondName,
                Password = new MD5Hash(password).Crypt(),
                Role = role
            };
        }
        public override User GetRequestResult()
        {
            GetResult<User> resultUser = RestHttpRequest<GetResult<User>, User>.Execute(baseUrl + "/account/reguser", RestMethods.Post, _user);
            if (resultUser != null)
            {
                return resultUser.Result;
            }
            return null;
        }
    }
}
