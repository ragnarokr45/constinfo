﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;

namespace ConstInfo.Result.RsultForForm.UserResults
{
    class GetUserResult : BaseResultForRorm<GetUserResult, List<User>>
    {
        public GetUserResult()
        {

        }
        public GetUserResult(bool deletedUsers)
        {
            showDeletedUsers = deletedUsers;
        }
        public override List<User> GetRequestResult()
        {
            GetResult<List<User>> resultUsers = RestHttpRequest<GetResult<List<User>>, string>.Execute(baseUrl + $"/account/users?showDeleted={showDeletedUsers}", RestMethods.Get);
            if (resultUsers != null)
            {
                return resultUsers.Result;
            }
            return new List<User>();
        }
    }
}
