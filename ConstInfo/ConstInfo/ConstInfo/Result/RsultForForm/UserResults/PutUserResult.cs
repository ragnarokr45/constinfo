﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.DataReceiver;
using ConstInfo.Models;
using ConstInfo.Cryptography;


namespace ConstInfo.Result.RsultForForm.UserResults
{
    class PutUserResult : BaseResultForRorm<PutUserResult, bool>
    {
        User _user;
        public PutUserResult(int id, string login, string firstName, string secondName, string password, Roles role)
        {
            _user = new User()
            {
                Id = id,
                Login = login,
                FirstName = firstName,
                SecondName = secondName,
                Password = (password == "" ? null : new MD5Hash(password).Crypt()),
                Role = role
            };
        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, User>.Execute(baseUrl + "/account/update", RestMethods.Put, _user);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
