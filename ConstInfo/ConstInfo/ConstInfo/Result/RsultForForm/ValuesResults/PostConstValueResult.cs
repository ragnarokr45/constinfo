﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.ValuesResults
{
    class PostConstValueResult : BaseResultForRorm<PostConstValueResult, ConstValue>
    {
        ConstValue _constValue;
        public PostConstValueResult(string name, string value, Branch branch, string remark, ConstType constType, User user = null)
        {
            _constValue = new ConstValue
            {
                Name = name,
                Value = value,
                Branch = branch,
                Remark = remark,
                Date = DateTime.Now,
                ConstType = constType,
                User = user ?? Storage.Instance.User
            };
        }
        public override ConstValue GetRequestResult()
        {
            GetResult<ConstValue> result = RestHttpRequest<GetResult<ConstValue>, ConstValue>.Execute(baseUrl + "/constvalue/add", RestMethods.Post, _constValue);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
