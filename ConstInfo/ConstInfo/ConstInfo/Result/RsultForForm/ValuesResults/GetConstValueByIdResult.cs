﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.ValuesResults
{
    class GetConstValueByIdResult : BaseResultForRorm<GetConstValueByIdResult, ConstValue>
    {
        int _id;
        public GetConstValueByIdResult(int id)
        {
            _id = id;
        }
        public override ConstValue GetRequestResult()
        {
            GetResult<ConstValue> result = RestHttpRequest<GetResult<ConstValue>, string>.Execute(baseUrl + $"/constvalue/onevalue?id={_id}", RestMethods.Get);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
