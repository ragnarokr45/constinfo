﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.ValuesResults
{
    class GetCanChangeConstValue : BaseResultForRorm<GetCanChangeConstValue, bool>
    {
        ConstValue _constValue;
        public GetCanChangeConstValue(ConstValue constValue)
        {
            _constValue = constValue;
        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, ConstValue>.Execute(baseUrl + "/constvalue/canupdate", RestMethods.Post, _constValue);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
