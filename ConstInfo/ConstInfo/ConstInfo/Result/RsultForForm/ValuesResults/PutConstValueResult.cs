﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.ValuesResults
{
    class PutConstValueResult : BaseResultForRorm<PutConstValueResult, bool>
    {
        ConstValue _constValue;
        public PutConstValueResult(ConstValue constValue)
        {
            _constValue = constValue;
        }
        public PutConstValueResult(int id, string name, string value, Branch branch, string remark, DateTime date, ConstType constType, User user)
        {
            _constValue = new ConstValue
            {
                ValueId = id,
                Name = name,
                Value = value,
                Branch = branch,
                Remark = remark,
                Date = date,
                ConstType = constType,
                User = user
            };
        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, ConstValue>.Execute(baseUrl + "/constvalue/update", RestMethods.Put, _constValue);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
