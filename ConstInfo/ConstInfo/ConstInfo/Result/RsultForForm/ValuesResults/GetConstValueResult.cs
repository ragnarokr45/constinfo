﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.ValuesResults
{
    class GetConstValueResult : BaseResultForRorm<GetConstValueResult, List<ConstValue>>
    {
        int _id;
        public GetConstValueResult(int id)
        {
            _id = id;
        }
        public override List<ConstValue> GetRequestResult()
        {
            GetResult<List<ConstValue>> result = RestHttpRequest<GetResult<List<ConstValue>>, string>.Execute(baseUrl + $"/constvalue/values?id={_id}&showDeleted={showDeletedValues}", RestMethods.Get);
            if (result != null)
            {
                return result.Result;
            }
            return new List<ConstValue>();
        }
    }
}
