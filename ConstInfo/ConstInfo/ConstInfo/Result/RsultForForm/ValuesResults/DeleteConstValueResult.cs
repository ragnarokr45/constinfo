﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;
using ConstInfo.Result;

namespace ConstInfo.Result.RsultForForm.ValuesResults
{
    class DeleteConstValueResult : BaseResultForRorm<DeleteConstValueResult, bool>
    {
        int _id;
        bool _delete;

        public DeleteConstValueResult(int id, bool delete)
        {
            _id = id;
            _delete = delete;
        }

        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, string>.Execute(baseUrl + $"/constvalue/changestate?id={_id}&delete={_delete}", RestMethods.Delete);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
