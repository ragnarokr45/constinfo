﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Result.RsultForForm
{
    abstract class BaseResultForRorm<T, O> where T : BaseResultForRorm<T, O>
    {
        protected string baseUrl = Properties.Settings.Default["BaseUrl"].ToString();
        protected bool showDeletedUsers = Storage.Instance.ShowDeletedUsers;
        protected bool showDeletedValues = Storage.Instance.ShowDeletedValues;
        protected bool showDeletedBranches = Storage.Instance.ShowDeletedBranches;
        public abstract O GetRequestResult();

        private O ExecuteResult()
        {
            return GetRequestResult();
        }

        public static O Execute(params object[] values)
        {
            var operation = (T)Activator.CreateInstance(typeof(T), values);
            return operation.ExecuteResult();
        }
    }
}
