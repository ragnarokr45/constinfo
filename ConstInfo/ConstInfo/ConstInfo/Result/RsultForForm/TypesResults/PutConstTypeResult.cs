﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.DataReceiver;
using ConstInfo.Models;

namespace ConstInfo.Result.RsultForForm.TypesResults
{
    class PutConstTypeResult : BaseResultForRorm<PutConstTypeResult, bool>
    {
        ConstType _constType;

        public PutConstTypeResult(int id, string name)
        {
            _constType = new ConstType()
            {
                Id = id,
                ConstName = name,
            };

        }
        
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, ConstType>.Execute(baseUrl + "/consttype/update", RestMethods.Put, _constType);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
