﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;

namespace ConstInfo.Result.RsultForForm.TypesResults
{
    class DeleteConstTypeResult : BaseResultForRorm<DeleteConstTypeResult, bool>
    {
        int _id;
        bool _delete;
        public DeleteConstTypeResult(int id, bool delete)
        {
            _id = id;
            _delete = delete;
        }
        public override bool GetRequestResult()
        {
            BaseResult result = RestHttpRequest<BaseResult, string>.Execute(baseUrl + $"/consttype/changestate?id={_id}&delete={_delete}", RestMethods.Delete);
            if (result != null)
            {
                return true;
            }
            return false;
        }
    }
}
