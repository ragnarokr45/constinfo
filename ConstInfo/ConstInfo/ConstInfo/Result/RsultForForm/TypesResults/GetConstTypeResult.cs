﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;


namespace ConstInfo.Result.RsultForForm.TypesResults
{
    class GetConstTypeResult : BaseResultForRorm<GetConstTypeResult, List<ConstType>>
    {
        public override List<ConstType> GetRequestResult()
        {
            GetResult<List<ConstType>> result = RestHttpRequest<GetResult<List<ConstType>>, string>.Execute(baseUrl + $"/consttype/types?showdeleted={showDeletedValues}", RestMethods.Get);

            if (result != null)
            {
                return result.Result;
            }
            return new List<ConstType>();
        }
    }
}
