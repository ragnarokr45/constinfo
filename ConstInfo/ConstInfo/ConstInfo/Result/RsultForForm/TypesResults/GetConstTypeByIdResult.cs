﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;

namespace ConstInfo.Result.RsultForForm.TypesResults
{
    class GetConstTypeByIdResult : BaseResultForRorm<GetConstTypeByIdResult, ConstType>
    {
        int _id;
        public GetConstTypeByIdResult(int id)
        {
            _id = id;
        }
        public override ConstType GetRequestResult()
        {
            GetResult<ConstType> result = RestHttpRequest<GetResult<ConstType>, string>.Execute(baseUrl + $"/consttype/onetype?id={_id}", RestMethods.Get);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
