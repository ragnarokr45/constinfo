﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Models;
using ConstInfo.DataReceiver;

namespace ConstInfo.Result.RsultForForm.TypesResults
{
    class PostConstTypeResult : BaseResultForRorm<PostConstTypeResult, ConstType>
    {
        ConstType _constType;
        public PostConstTypeResult(string name)
        {
            _constType = new ConstType()
            {
                ConstName = name
            };
        }

        public override ConstType GetRequestResult()
        {
            GetResult<ConstType> result = RestHttpRequest<GetResult<ConstType>, ConstType>.Execute(baseUrl + "/consttype/add", RestMethods.Post, _constType);
            if (result != null)
            {
                return result.Result;
            }
            return null;
        }
    }
}
