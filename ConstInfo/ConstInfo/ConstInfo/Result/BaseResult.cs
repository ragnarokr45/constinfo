﻿using ConstInfo.Alerts.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Result
{
    [DataContract]
    public class BaseResult
    {
        [DataMember(Name = "errorCode")]
        public ErrorCode ErrorCode { get; set; }
        [DataMember(Name = "errorDescription")]
        public string ErrorDescription { get; set; }

    }
}
