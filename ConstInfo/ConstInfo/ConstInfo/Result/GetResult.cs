﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Result
{
    [DataContract]
    public class GetResult<T> : BaseResult
    {
        [DataMember(Name = "result")]
        public T Result { get; set; }
    }
}
