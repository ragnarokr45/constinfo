﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.DataReceiver;
using ConstInfo.Result;
using ConstInfo.Models;
using ConstInfo.Result.RsultForForm.UserResults;

namespace ConstInfo
{
    public partial class AddUserForm : Form
    {
        public User user;
        public AddUserForm()
        {
            InitializeComponent();
            roleComboBox.DataSource = Enum.GetNames(typeof(Roles));
            roleComboBox.SelectedIndex = roleComboBox.FindStringExact("user");
        }

        private void AddUser()
        {
            user = PostUserResult.Execute(
                loginTextBox.Text, 
                firstNameTextBox.Text, 
                secondNameTextBox.Text, 
                passwordTextBox.Text, 
                (Roles)Enum.Parse(typeof(Roles), roleComboBox.Text));
            if (user != null)
            {
                this.Close();
            }
        }

        private void addUserButton_Click(object sender, EventArgs e)
        {
            AddUser();
        }
    }
}
