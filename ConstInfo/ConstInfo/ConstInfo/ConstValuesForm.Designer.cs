﻿namespace ConstInfo
{
    partial class ConstValuesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConstValuesForm));
            this.constValuesGridView = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brachesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.constTypeListView = new System.Windows.Forms.ListView();
            this.constTyprColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.addConstTypeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.updateConstTypeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteConstTypeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.restoreTypeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.addConstValueToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.updateConstValueToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.deleteConstValueToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.restoreValueToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.refreshConstToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.showDeletedToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.constTypeContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addConstTypeTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateConstTypeTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteConstTypeTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returnConstTypeTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constValueContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addConstValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateConstValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteConstValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returnConstValueValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.constValuesGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.constTypeContextMenuStrip.SuspendLayout();
            this.constValueContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // constValuesGridView
            // 
            this.constValuesGridView.AllowUserToAddRows = false;
            this.constValuesGridView.AllowUserToDeleteRows = false;
            this.constValuesGridView.AllowUserToResizeColumns = false;
            this.constValuesGridView.AllowUserToResizeRows = false;
            this.constValuesGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.constValuesGridView.BackgroundColor = System.Drawing.Color.White;
            this.constValuesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.constValuesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4,
            this.Column5,
            this.Column3,
            this.Column6,
            this.Column1,
            this.Column7});
            this.constValuesGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.constValuesGridView.Location = new System.Drawing.Point(0, 0);
            this.constValuesGridView.MultiSelect = false;
            this.constValuesGridView.Name = "constValuesGridView";
            this.constValuesGridView.ReadOnly = true;
            this.constValuesGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.constValuesGridView.Size = new System.Drawing.Size(965, 455);
            this.constValuesGridView.TabIndex = 2;
            this.constValuesGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.constValuesGridView_CellMouseClick);
            this.constValuesGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.constValuesGridView_RowHeaderMouseClick);
            this.constValuesGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.constValuesGridView_MouseDown);
            this.constValuesGridView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.constValuesGridView_MouseMove);
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Name";
            this.Column4.HeaderText = "Name";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Value";
            this.Column5.HeaderText = "Value";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Date";
            this.Column3.HeaderText = "Date";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Branch";
            this.Column6.HeaderText = "Branch";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "User";
            this.Column1.HeaderText = "User";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "Remark";
            this.Column7.HeaderText = "Remark";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1135, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logOutToolStripMenuItem,
            this.toolStripMenuItem1,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.logOutToolStripMenuItem.Text = "LogOut";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(111, 6);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.usersToolStripMenuItem,
            this.brachesToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.usersToolStripMenuItem.Text = "Users";
            this.usersToolStripMenuItem.Visible = false;
            this.usersToolStripMenuItem.Click += new System.EventHandler(this.usersToolStripMenuItem_Click);
            // 
            // brachesToolStripMenuItem
            // 
            this.brachesToolStripMenuItem.Name = "brachesToolStripMenuItem";
            this.brachesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.brachesToolStripMenuItem.Text = "Braches";
            this.brachesToolStripMenuItem.Click += new System.EventHandler(this.brachesToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1135, 480);
            this.splitContainer1.SplitterDistance = 166;
            this.splitContainer1.TabIndex = 9;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.constTypeListView);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 25);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(166, 455);
            this.panel4.TabIndex = 7;
            // 
            // constTypeListView
            // 
            this.constTypeListView.AllowDrop = true;
            this.constTypeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.constTyprColumn});
            this.constTypeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.constTypeListView.FullRowSelect = true;
            this.constTypeListView.Location = new System.Drawing.Point(0, 0);
            this.constTypeListView.MultiSelect = false;
            this.constTypeListView.Name = "constTypeListView";
            this.constTypeListView.Size = new System.Drawing.Size(166, 455);
            this.constTypeListView.TabIndex = 3;
            this.constTypeListView.UseCompatibleStateImageBehavior = false;
            this.constTypeListView.View = System.Windows.Forms.View.Details;
            this.constTypeListView.SelectedIndexChanged += new System.EventHandler(this.constTypeListView_SelectedIndexChanged);
            this.constTypeListView.DragDrop += new System.Windows.Forms.DragEventHandler(this.constTypeListView_DragDrop);
            this.constTypeListView.DragEnter += new System.Windows.Forms.DragEventHandler(this.constTypeListView_DragEnter);
            this.constTypeListView.DragOver += new System.Windows.Forms.DragEventHandler(this.constTypeListView_DragOver);
            this.constTypeListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.constTypeListView_MouseClick);
            // 
            // constTyprColumn
            // 
            this.constTyprColumn.Text = "ConstType";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.toolStrip3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(166, 25);
            this.panel3.TabIndex = 6;
            this.panel3.Visible = false;
            // 
            // toolStrip3
            // 
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConstTypeToolStripButton,
            this.updateConstTypeToolStripButton,
            this.deleteConstTypeToolStripButton,
            this.restoreTypeToolStripButton});
            this.toolStrip3.Location = new System.Drawing.Point(0, 0);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(166, 25);
            this.toolStrip3.TabIndex = 0;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // addConstTypeToolStripButton
            // 
            this.addConstTypeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addConstTypeToolStripButton.Image = global::ConstInfo.Properties.Resources.add;
            this.addConstTypeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addConstTypeToolStripButton.Name = "addConstTypeToolStripButton";
            this.addConstTypeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.addConstTypeToolStripButton.Text = "Add";
            this.addConstTypeToolStripButton.Click += new System.EventHandler(this.addConstTypeToolStripButton_Click);
            // 
            // updateConstTypeToolStripButton
            // 
            this.updateConstTypeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.updateConstTypeToolStripButton.Image = global::ConstInfo.Properties.Resources.edit;
            this.updateConstTypeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.updateConstTypeToolStripButton.Name = "updateConstTypeToolStripButton";
            this.updateConstTypeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.updateConstTypeToolStripButton.Text = "Edit";
            this.updateConstTypeToolStripButton.Click += new System.EventHandler(this.updateConstTypeToolStripButton_Click);
            // 
            // deleteConstTypeToolStripButton
            // 
            this.deleteConstTypeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteConstTypeToolStripButton.Image = global::ConstInfo.Properties.Resources.delete;
            this.deleteConstTypeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteConstTypeToolStripButton.Name = "deleteConstTypeToolStripButton";
            this.deleteConstTypeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteConstTypeToolStripButton.Text = "Delete";
            this.deleteConstTypeToolStripButton.Click += new System.EventHandler(this.deleteConstTypeToolStripButton_Click);
            // 
            // restoreTypeToolStripButton
            // 
            this.restoreTypeToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.restoreTypeToolStripButton.Image = global::ConstInfo.Properties.Resources.new_rel;
            this.restoreTypeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.restoreTypeToolStripButton.Name = "restoreTypeToolStripButton";
            this.restoreTypeToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.restoreTypeToolStripButton.Text = "Restore";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.constValuesGridView);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(965, 455);
            this.panel2.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.toolStrip2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(965, 25);
            this.panel1.TabIndex = 4;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConstValueToolStripButton,
            this.updateConstValueToolStripButton,
            this.deleteConstValueToolStripButton,
            this.restoreValueToolStripButton,
            this.refreshConstToolStripButton,
            this.showDeletedToolStripButton});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(965, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // addConstValueToolStripButton
            // 
            this.addConstValueToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addConstValueToolStripButton.Image = global::ConstInfo.Properties.Resources.add;
            this.addConstValueToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addConstValueToolStripButton.Name = "addConstValueToolStripButton";
            this.addConstValueToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.addConstValueToolStripButton.Text = "Add";
            this.addConstValueToolStripButton.Click += new System.EventHandler(this.addConstValueToolStripButton_Click);
            // 
            // updateConstValueToolStripButton
            // 
            this.updateConstValueToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.updateConstValueToolStripButton.Image = global::ConstInfo.Properties.Resources.edit;
            this.updateConstValueToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.updateConstValueToolStripButton.Name = "updateConstValueToolStripButton";
            this.updateConstValueToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.updateConstValueToolStripButton.Text = "Edit";
            this.updateConstValueToolStripButton.Click += new System.EventHandler(this.updateConstValueToolStripButton_Click);
            // 
            // deleteConstValueToolStripButton
            // 
            this.deleteConstValueToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteConstValueToolStripButton.Image = global::ConstInfo.Properties.Resources.delete;
            this.deleteConstValueToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteConstValueToolStripButton.Name = "deleteConstValueToolStripButton";
            this.deleteConstValueToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.deleteConstValueToolStripButton.Text = "Delete";
            this.deleteConstValueToolStripButton.Click += new System.EventHandler(this.deleteConstValueToolStripButton_Click);
            // 
            // restoreValueToolStripButton
            // 
            this.restoreValueToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.restoreValueToolStripButton.Image = global::ConstInfo.Properties.Resources.new_rel;
            this.restoreValueToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.restoreValueToolStripButton.Name = "restoreValueToolStripButton";
            this.restoreValueToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.restoreValueToolStripButton.Text = "Restore";
            // 
            // refreshConstToolStripButton
            // 
            this.refreshConstToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshConstToolStripButton.Image = global::ConstInfo.Properties.Resources.refresh;
            this.refreshConstToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshConstToolStripButton.Name = "refreshConstToolStripButton";
            this.refreshConstToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.refreshConstToolStripButton.Text = "Refresh";
            this.refreshConstToolStripButton.Click += new System.EventHandler(this.refreshConstToolStripButton_Click);
            // 
            // showDeletedToolStripButton
            // 
            this.showDeletedToolStripButton.BackColor = System.Drawing.Color.White;
            this.showDeletedToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.showDeletedToolStripButton.Image = global::ConstInfo.Properties.Resources.view;
            this.showDeletedToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.showDeletedToolStripButton.Name = "showDeletedToolStripButton";
            this.showDeletedToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.showDeletedToolStripButton.Text = "Show deleted";
            this.showDeletedToolStripButton.Click += new System.EventHandler(this.showDeletedToolStripButton_Click);
            // 
            // constTypeContextMenuStrip
            // 
            this.constTypeContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConstTypeTypeToolStripMenuItem,
            this.updateConstTypeTypeToolStripMenuItem,
            this.deleteConstTypeTypeToolStripMenuItem,
            this.returnConstTypeTypeToolStripMenuItem});
            this.constTypeContextMenuStrip.Name = "constTypeContextMenuStrip";
            this.constTypeContextMenuStrip.Size = new System.Drawing.Size(114, 92);
            // 
            // addConstTypeTypeToolStripMenuItem
            // 
            this.addConstTypeTypeToolStripMenuItem.Name = "addConstTypeTypeToolStripMenuItem";
            this.addConstTypeTypeToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.addConstTypeTypeToolStripMenuItem.Text = "Add";
            this.addConstTypeTypeToolStripMenuItem.Click += new System.EventHandler(this.addConstTypeTypeToolStripMenuItem_Click);
            // 
            // updateConstTypeTypeToolStripMenuItem
            // 
            this.updateConstTypeTypeToolStripMenuItem.Name = "updateConstTypeTypeToolStripMenuItem";
            this.updateConstTypeTypeToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.updateConstTypeTypeToolStripMenuItem.Text = "Update";
            this.updateConstTypeTypeToolStripMenuItem.Click += new System.EventHandler(this.updateConstTypeTypeToolStripMenuItem_Click);
            // 
            // deleteConstTypeTypeToolStripMenuItem
            // 
            this.deleteConstTypeTypeToolStripMenuItem.Name = "deleteConstTypeTypeToolStripMenuItem";
            this.deleteConstTypeTypeToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.deleteConstTypeTypeToolStripMenuItem.Text = "Delete";
            this.deleteConstTypeTypeToolStripMenuItem.Click += new System.EventHandler(this.deleteConstTypeTypeToolStripMenuItem_Click);
            // 
            // returnConstTypeTypeToolStripMenuItem
            // 
            this.returnConstTypeTypeToolStripMenuItem.Name = "returnConstTypeTypeToolStripMenuItem";
            this.returnConstTypeTypeToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.returnConstTypeTypeToolStripMenuItem.Text = "Restore";
            this.returnConstTypeTypeToolStripMenuItem.Click += new System.EventHandler(this.returnTypeToolStripMenuItem_Click);
            // 
            // constValueContextMenuStrip
            // 
            this.constValueContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConstValueToolStripMenuItem,
            this.updateConstValueToolStripMenuItem,
            this.deleteConstValueToolStripMenuItem,
            this.returnConstValueValueToolStripMenuItem});
            this.constValueContextMenuStrip.Name = "constValueContextMenuStrip";
            this.constValueContextMenuStrip.Size = new System.Drawing.Size(114, 92);
            // 
            // addConstValueToolStripMenuItem
            // 
            this.addConstValueToolStripMenuItem.Name = "addConstValueToolStripMenuItem";
            this.addConstValueToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.addConstValueToolStripMenuItem.Text = "Add";
            this.addConstValueToolStripMenuItem.Click += new System.EventHandler(this.addConstValueToolStripMenuItem_Click);
            // 
            // updateConstValueToolStripMenuItem
            // 
            this.updateConstValueToolStripMenuItem.Name = "updateConstValueToolStripMenuItem";
            this.updateConstValueToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.updateConstValueToolStripMenuItem.Text = "Update";
            this.updateConstValueToolStripMenuItem.Click += new System.EventHandler(this.updateConstValueToolStripMenuItem_Click);
            // 
            // deleteConstValueToolStripMenuItem
            // 
            this.deleteConstValueToolStripMenuItem.Name = "deleteConstValueToolStripMenuItem";
            this.deleteConstValueToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.deleteConstValueToolStripMenuItem.Text = "Delete";
            this.deleteConstValueToolStripMenuItem.Click += new System.EventHandler(this.deleteConstValueToolStripMenuItem_Click);
            // 
            // returnConstValueValueToolStripMenuItem
            // 
            this.returnConstValueValueToolStripMenuItem.Name = "returnConstValueValueToolStripMenuItem";
            this.returnConstValueValueToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.returnConstValueValueToolStripMenuItem.Text = "Restore";
            this.returnConstValueValueToolStripMenuItem.Click += new System.EventHandler(this.returnValueToolStripMenuItem_Click);
            // 
            // ConstValuesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(1135, 504);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ConstValuesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Const info";
            this.Load += new System.EventHandler(this.ConstValuesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.constValuesGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.constTypeContextMenuStrip.ResumeLayout(false);
            this.constValueContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView constValuesGridView;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton addConstTypeToolStripButton;
        private System.Windows.Forms.ToolStripButton updateConstTypeToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteConstTypeToolStripButton;
        private System.Windows.Forms.ToolStripButton addConstValueToolStripButton;
        private System.Windows.Forms.ToolStripButton updateConstValueToolStripButton;
        private System.Windows.Forms.ToolStripButton deleteConstValueToolStripButton;
        private System.Windows.Forms.ToolStripButton refreshConstToolStripButton;
        private System.Windows.Forms.ToolStripButton showDeletedToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brachesToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip constTypeContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem returnConstTypeTypeToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip constValueContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem returnConstValueValueToolStripMenuItem;
        private System.Windows.Forms.ListView constTypeListView;
        private System.Windows.Forms.ColumnHeader constTyprColumn;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addConstTypeTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateConstTypeTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteConstTypeTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addConstValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateConstValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteConstValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton restoreTypeToolStripButton;
        private System.Windows.Forms.ToolStripButton restoreValueToolStripButton;
    }
}