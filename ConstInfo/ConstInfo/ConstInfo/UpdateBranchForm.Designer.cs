﻿namespace ConstInfo
{
    partial class UpdateBranchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.updateBranchButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(60, 20);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(115, 20);
            this.nameTextBox.TabIndex = 0;
            // 
            // updateBranchButton
            // 
            this.updateBranchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateBranchButton.Location = new System.Drawing.Point(60, 50);
            this.updateBranchButton.Name = "updateBranchButton";
            this.updateBranchButton.Size = new System.Drawing.Size(115, 22);
            this.updateBranchButton.TabIndex = 1;
            this.updateBranchButton.Text = "Update branch";
            this.updateBranchButton.UseVisualStyleBackColor = true;
            this.updateBranchButton.Click += new System.EventHandler(this.updateBranchButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name";
            // 
            // UpdateBranchForm
            // 
            this.AcceptButton = this.updateBranchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(206, 95);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.updateBranchButton);
            this.Controls.Add(this.nameTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(222, 134);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(222, 134);
            this.Name = "UpdateBranchForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update branch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button updateBranchButton;
        private System.Windows.Forms.Label label1;
    }
}