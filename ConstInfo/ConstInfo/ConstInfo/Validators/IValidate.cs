﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Validators
{
    interface IValidate
    {
        bool Validate();
    }
}
