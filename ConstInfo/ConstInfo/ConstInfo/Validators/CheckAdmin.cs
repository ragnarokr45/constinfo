﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Validators
{
    class CheckAdmin : IValidate
    {
        public bool Validate()
        {
            if (Storage.Instance.User.Role == Models.Roles.admin)
            {
                return true;
            }
            return false;
        }
    }
}
