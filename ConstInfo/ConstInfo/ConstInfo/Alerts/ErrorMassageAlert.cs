﻿using ConstInfo.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.Alerts.Errors;

namespace ConstInfo.Alerts
{
    class ErrorMassageAlert : BaseAlert<ErrorMassageAlert, bool>
    {
        string _errorDescription = String.Empty;
        public ErrorMassageAlert()
        {

        }
        public ErrorMassageAlert(string errorDescription)
        {
            _errorDescription = errorDescription;
        }
        public override bool Alert()
        {
            if (errorCode == ErrorCode.CustomError)
            {
                MessageBox.Show(_errorDescription);
                return false;
            }
            else if (errorCode != ErrorCode.Success)
            {
                MessageBox.Show(ErrorDescription.description[errorCode]);
                return false;
            }
            return true;
        }
    }
}
