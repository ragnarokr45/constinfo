﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.Alerts.Errors
{
    public static class ErrorDescription
    {
        public static Dictionary<ErrorCode, string> description = new Dictionary<ErrorCode, string>
        {
            {ErrorCode.InputError, "Invalid input data"},
            {ErrorCode.AuthenticationError, "The user login or password is incorrect."},
            {ErrorCode.OwnerError, "You are trying to change other people's data."},
            {ErrorCode.BearerError, "Invalid bearer token."},
            {ErrorCode.RoleError, "You don't have permissions to do that."},
            {ErrorCode.UserExistenceError, "This user has been deleted."},
            {ErrorCode.SelectedItemError, "Select one item."},
            {ErrorCode.LoginError, "This user alredy exist"}
        };
    }
}
