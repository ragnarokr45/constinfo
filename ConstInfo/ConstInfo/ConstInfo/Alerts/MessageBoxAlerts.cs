﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConstInfo.Alerts
{
    public static class MessageBoxAlerts
    {
        public static  bool SureMessage()
        {
            DialogResult mess = MessageBox.Show("Are you sure? ", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (mess == DialogResult.Yes)
            {
                return true;
            }
            return false;
        }
        public static bool ValuesDeletedMessage()
        {
            DialogResult messDelValue = MessageBox.Show("All input values ​​will be deleted! ", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (messDelValue == DialogResult.OK)
            {
                return true;
            }
            return false;
        }
    }
}
