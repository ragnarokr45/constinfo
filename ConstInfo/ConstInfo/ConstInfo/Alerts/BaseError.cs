﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Alerts.Errors;

namespace ConstInfo.Alerts
{
    public abstract class BaseAlert<T, O> where T: BaseAlert<T, O>
    {
        protected ErrorCode errorCode;
        public abstract O Alert();

        public O ExecuteAlert()
        {
            return Alert();
        }

        public static O Execute(ErrorCode ec, params object[] values)
        {
            var al = (T)Activator.CreateInstance(typeof(T), values);
            al.errorCode = ec;
            return al.ExecuteAlert();
        }
    }
}
