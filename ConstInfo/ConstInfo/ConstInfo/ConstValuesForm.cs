﻿using ConstInfo.Models;
using ConstInfo.Result;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.DataReceiver;
using ConstInfo.Validators;
using ConstInfo.Result.RsultForForm.TypesResults;
using ConstInfo.Result.RsultForForm.ValuesResults;
using ConstInfo.Alerts;
using ConstInfo.Alerts.Errors;

namespace ConstInfo
{
    public partial class ConstValuesForm : Form
    {
        List<ConstType> _constTypes;
        List<ConstValue> _constValues;
        Storage store = Storage.Instance;
        bool _adminAuthority = false;
        int _selectedDelItem = -1;
        int _columnValue = 1;
        int _selectedIdType = 0;
        int _selectedIdValue = 0;
        int _sortedColumn = 2;
        public ConstValuesForm()
        {          
            InitializeComponent();
        }

        private void ShowLoginform()
        {
            store.User = null;
            this.Hide();
            LoginForm loginForm = new LoginForm();
            if (loginForm.ShowDialog() == DialogResult.Cancel)
            {
                this.Close();
            }
            else
            {
                this.Show();
                FormItemsSettings();
                store.ShowDeletedValues = false;
                AdminMenu();
                GetData();
            }
        }

        private void FormItemsSettings()
        {
            showDeletedToolStripButton.BackColor = store.redColor;
            constTypeListView.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            constTypeListView.AutoResizeColumn(0,
        ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        private int GetTypeIdByName()
        {
            return _constTypes.Find(x => x.Id == (int)constTypeListView.SelectedItems[0].Tag)?.Id ?? -1;
        }

        private void AdminMenu()
        {
            var adminValidator = new CheckAdmin();
            if (adminValidator.Validate())
            {
                panel3.Visible = true;
                usersToolStripMenuItem.Visible = true;
                _adminAuthority = true;
            }
        }

        public void GetData()
        {
            _selectedDelItem = -1;
            _constTypes = new List<ConstType>();
            _constTypes = GetConstTypeResult.Execute();
            ShowConstType();
        }

        private void ShowConstType()
        {
            constTypeListView.Items.Clear();
            _constTypes = _constTypes.OrderBy(x => x.ConstName).ToList();
            for (int i = 0; i < _constTypes.Count; i++)
            {
                constTypeListView.Items.Add(CreateListViewItem(_constTypes[i]));
                if (_constTypes[i].IsDeleted)
                {
                    constTypeListView.Items[constTypeListView.Items.Count - 1].BackColor = store.redColor;
                }               
            }
            var ct = constTypeListView.Items.Cast<ListViewItem>().ToList().Find(x => (int)x.Tag == _selectedIdType);
            if (ct != null)
            {
                ct.Selected = true;
            }
            else
            {
                _selectedIdType = 0;
                var listItem = constTypeListView.Items.Cast<ListViewItem>().ToList().FirstOrDefault();
                if (listItem != null)
                {
                    listItem.Selected = true;
                }
            }
        }

        private ListViewItem CreateListViewItem(ConstType constType)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = constType.ConstName;
            lvi.Tag = constType.Id;
            return lvi;
        }

        private void ShowConstValues()
        {
            _constValues = new List<ConstValue>();
            if (_selectedIdType != 0)
            {
                _constValues.AddRange(GetConstValueResult.Execute(_selectedIdType));
                constValuesGridView.Rows.Clear();
                foreach (var item in _constValues)
                {
                    AddRow(item);
                }
                var cv = constValuesGridView.Rows.Cast<DataGridViewRow>().ToList().Find(x => (int)x.Tag == _selectedIdValue);
                if (cv != null)
                {
                    cv.Cells[0].Selected = true;
                    constValuesGridView.Rows[0].Cells[0].Selected = false;
                    cv.Selected = true;

                }
                else
                {
                    var itemIdGridView = constValuesGridView.Rows.GetFirstRow(0);
                    if (itemIdGridView != -1)
                    {
                        _selectedIdValue = (int)constValuesGridView.Rows[itemIdGridView].Tag;
                        constValuesGridView.Rows[itemIdGridView].Selected = true;
                        constValuesGridView.Rows[itemIdGridView].Cells[0].Selected = true;
                    }
                }
            }
        }

        private void AddRow(ConstValue constValue)
        {
            DataGridViewRow dgvRow = new DataGridViewRow();
            dgvRow.CreateCells(constValuesGridView);
            dgvRow.Cells[0].Value = constValue.Name;
            dgvRow.Cells[1].Value = constValue.Value;
            dgvRow.Cells[2].Value = constValue.Date.ToString("dd/MM/yyyy");
            dgvRow.Cells[3].Value = constValue.Branch.BranchName;
            dgvRow.Cells[4].Value = constValue.User.SecondName == "" && constValue.User.FirstName == "" ? 
                constValue.User.Login : 
                $"{constValue.User.SecondName} {constValue.User.FirstName}";
            dgvRow.Cells[5].Value = constValue.Remark;
            dgvRow.Tag = constValue.ValueId;
            constValuesGridView.Rows.Add(dgvRow);
            if (constValue.IsDeleted)
            {
                constValuesGridView.Rows[constValuesGridView.Rows.Count - 1].DefaultCellStyle.BackColor = store.redColor;
            }
        }

        private ConstValue SearchConstValueById()
        {
            int idConstValue = (int)constValuesGridView.Rows[constValuesGridView.SelectedCells[0].RowIndex].Tag;
            int idConstType = _constTypes.Find(v => v.Id == (int)constTypeListView.SelectedItems[0].Tag).Id;
            return _constValues.Find(c => c.ConstType.Id == idConstType && c.ValueId == idConstValue);
        }

        private void DeleteConstValue()
        {
            if (constValuesGridView.SelectedCells.Count != 0)
            {
                ConstValue cv = SearchConstValueById();
                if (!cv.IsDeleted && (_adminAuthority || GetCanChangeConstValue.Execute(cv)))
                {
                    if (MessageBoxAlerts.SureMessage())
                    {
                        DeleteConstValueResult.Execute(cv.ValueId, true);
                        GetData();
                    }
                }
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void DeleteConstType()
        {
            if (constTypeListView.SelectedItems[0].Text != null)
            {
                ConstType ct = _constTypes.Find(c => c.Id == (int)constTypeListView.SelectedItems[0].Tag);
                if (!ct.IsDeleted)
                {
                    if (MessageBoxAlerts.SureMessage())
                    {
                        List<ConstValue> delValues = _constValues.Where(x => x.ConstType.Id == ct.Id).ToList();
                        if (delValues.Count > 0)
                        {
                            if (MessageBoxAlerts.ValuesDeletedMessage())
                            {
                                DeleteConstTypeResult.Execute(ct.Id, true);
                                GetData();
                            }
                        }
                        else
                        {
                            DeleteConstTypeResult.Execute(ct.Id, true);
                            GetData();
                        }
                    }
                }
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void UpdateConstType()
        {
            if (constTypeListView.SelectedItems[0].Text != null)
            {
                var sendConstType = _constTypes.Find(c => c.Id == (int)constTypeListView.SelectedItems[0].Tag);
                if (!sendConstType.IsDeleted)
                {
                    UpdateConstTypeForm updateConstTypeForm = new UpdateConstTypeForm(sendConstType.Id);
                    updateConstTypeForm.ShowDialog();
                    GetData();
                }
                GetData();
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void UpdateConstValue()
        {
            if (constValuesGridView.SelectedCells.Count != 0)
            {
                ConstValue cv = SearchConstValueById();
                if (!cv.IsDeleted && (_adminAuthority || GetCanChangeConstValue.Execute(cv)))
                {
                    UpdateConstValueForm updateConstValueForm = new UpdateConstValueForm(cv.ValueId);
                    updateConstValueForm.ShowDialog();
                }
                GetData();
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }

        private void AddConstType()
        {
            AddConstTypeForm addConstTypeForm = new AddConstTypeForm();
            addConstTypeForm.ShowDialog();
            if (addConstTypeForm.constType != null)
            {
                _selectedIdType = addConstTypeForm.constType.Id;
            }
            GetData();
        }

        private void AddConstValue()
        {
            var sendConstType = _constTypes.Find(c => c.Id == (int)constTypeListView.SelectedItems[0].Tag);
            AddConstValueForm addConstValueForm = new AddConstValueForm(sendConstType);
            addConstValueForm.ShowDialog();
            if (addConstValueForm.constValue != null)
            {
                _selectedIdValue = addConstValueForm.constValue.ValueId;
            }
            GetData();
        }

        private void RestoreConstType()
        {
            ConstType ct = _constTypes.Find(x => x.Id == (int)constTypeListView.SelectedItems[0].Tag);
            DeleteConstTypeResult.Execute(ct.Id, false);
            GetData();
        }

        private void RestoreConstValue()
        {
            if (constValuesGridView.SelectedCells.Count != 0)
            {
                ConstValue cv = SearchConstValueById();
                DeleteConstValueResult.Execute(cv.ValueId, false);
                GetData();
            }
            else
            {
                ErrorMassageAlert.Execute(ErrorCode.SelectedItemError);
            }
        }
   
        private void addConstValueToolStripButton_Click(object sender, EventArgs e)
        {
            AddConstValue();
        }

        private void updateConstValueToolStripButton_Click(object sender, EventArgs e)
        {
            UpdateConstValue();
        }

        private void deleteConstValueToolStripButton_Click(object sender, EventArgs e)
        {
            DeleteConstValue();
        }

        private void addConstTypeToolStripButton_Click(object sender, EventArgs e)
        {
            AddConstType();
        }

        private void updateConstTypeToolStripButton_Click(object sender, EventArgs e)
        {
            UpdateConstType();
        }

        private void deleteConstTypeToolStripButton_Click(object sender, EventArgs e)
        {
            DeleteConstType();
        }

        private void refreshConstToolStripButton_Click(object sender, EventArgs e)
        {
            GetData();
        }

        private void showDeletedToolStripButton_Click(object sender, EventArgs e)
        {
            store.ShowDeletedValues = !store.ShowDeletedValues;
            if (store.ShowDeletedValues) showDeletedToolStripButton.BackColor = store.greenColor;
            else
            {
                showDeletedToolStripButton.BackColor = store.redColor;
                _selectedDelItem = -1;
            }
            GetData();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            SettingsForm settingsForm = new SettingsForm();
            settingsForm.ShowDialog();
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersForm usersForm = new UsersForm();
            usersForm.Show();
        }

        private void brachesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BranchesForm branchesForm = new BranchesForm();
            branchesForm.Show();
        }

        private void returnTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestoreConstType();
        }

        private void returnValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RestoreConstValue();
        }

        private void constTypeListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (constTypeListView.SelectedItems.Count == 0)
                return;
            this.constTypeListView.Items.Cast<ListViewItem>()
                .ToList().ForEach(item =>
                    {
                        if (item.BackColor != store.redColor)
                        {
                            item.BackColor = SystemColors.Window;
                        }
                        item.ForeColor = SystemColors.WindowText;
                        if (_selectedDelItem != -1)
                        {
                            constTypeListView.Items[_selectedDelItem].BackColor = store.redColor;
                            _selectedDelItem = -1;
                        }
                    });
            this.constTypeListView.SelectedItems.Cast<ListViewItem>()
                .ToList().ForEach(item =>
                    {
                        if (item.BackColor == store.redColor)
                        {
                            _selectedDelItem = item.Index;
                        }
                        item.BackColor = SystemColors.Highlight;
                        item.ForeColor = SystemColors.HighlightText;
                    });
            _selectedIdType = (int)constTypeListView.SelectedItems[0].Tag;
            ShowConstValues();
        }

        private void constTypeListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && _adminAuthority)
            {
                if (constTypeListView.SelectedItems[0].Bounds.Contains(e.Location))
                {
                    if (_constTypes.Find(x => x.Id == (int)constTypeListView.SelectedItems[0].Tag).IsDeleted)
                        returnConstTypeTypeToolStripMenuItem.Enabled = true;
                    else
                        returnConstTypeTypeToolStripMenuItem.Enabled = false;
                    constTypeContextMenuStrip.Show(Cursor.Position);
                }
            }
        }

        #region drag/drop
        private Rectangle dragBoxFromMouseDown;
        private ConstValue valueFromMouseDown;

        private void constValuesGridView_MouseDown(object sender, MouseEventArgs e)
        {
            var hittestInfo = constValuesGridView.HitTest(e.X, e.Y);

            if (hittestInfo.RowIndex != -1 && hittestInfo.ColumnIndex != -1)
            {
                string value = constValuesGridView.Rows[hittestInfo.RowIndex].Cells[_columnValue].Value.ToString();
                int selectedrowindex = constValuesGridView.Rows[hittestInfo.RowIndex].Cells[_columnValue].RowIndex;
                valueFromMouseDown = _constValues.Find(x => x.Value == value);
                if (valueFromMouseDown != null)
                {
                    Size dragSize = SystemInformation.DragSize;

                    dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
                }
            }
            else
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void constValuesGridView_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    DragDropEffects dropEffect = constValuesGridView.DoDragDrop(valueFromMouseDown, DragDropEffects.Copy);
                }
            }
        }

        private void constTypeListView_DragDrop(object sender, DragEventArgs e)
        {
            Point clientPoint = constTypeListView.PointToClient(new Point(e.X, e.Y));

            if (e.Effect == DragDropEffects.Copy)
            {
                ConstValue constValue = e.Data.GetData(typeof(ConstValue)) as ConstValue;
                var hittest = constTypeListView.HitTest(clientPoint.X, clientPoint.Y);
                if (hittest.Item != null && constValue != null && constValue.ConstType.ConstName != hittest.Item.Text)
                {
                    DialogResult mess = MessageBox.Show( 
                        $"Move value: {constValue.Value} from type: {constValue.ConstType.ConstName} to type: {hittest.Item.Text}?", 
                        "Confirmation", MessageBoxButtons.YesNo);
                    if (mess == DialogResult.Yes)
                    {
                        constValue.ConstType = _constTypes.Find(x => x.ConstName == hittest.Item.Text);
                        PutConstValueResult.Execute(constValue);
                        GetData();
                    }
                }
            }
        }

        private void constTypeListView_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void constTypeListView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
        #endregion

        private void constValuesGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                _selectedIdValue = (int)constValuesGridView.Rows[e.RowIndex].Tag;
            }
            else if (e.Button == MouseButtons.Right && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                constValuesGridView.CurrentCell = constValuesGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];
                constValuesGridView.Rows[e.RowIndex].Selected = true;
                if (constValuesGridView.Bounds.Contains(e.Location))
                {
                    if (_adminAuthority && _constValues.Find(x => x.Value == constValuesGridView.SelectedRows[0].Cells[_columnValue].Value.ToString()).IsDeleted)
                        returnConstValueValueToolStripMenuItem.Enabled = true;
                    else
                        returnConstValueValueToolStripMenuItem.Enabled = false;
                    constValueContextMenuStrip.Show(Cursor.Position);
                }
            }
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowLoginform();
        }

        private void ConstValuesForm_Load(object sender, EventArgs e)
        {
            ShowLoginform();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void constValuesGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                _selectedIdValue = (int)constValuesGridView.SelectedRows[0].Tag;
            }
        }

        private void addConstTypeTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddConstValue();
        }

        private void updateConstTypeTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateConstType();
        }

        private void deleteConstTypeTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteConstType();
        }

        private void addConstValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddConstValue();
        }

        private void updateConstValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateConstValue();
        }

        private void deleteConstValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteConstValue();
        }

        private void restoreTypeToolStripButton_Click(object sender, EventArgs e)
        {
            RestoreConstType();
        }

        private void restoreValueToolStripButton_Click(object sender, EventArgs e)
        {
            RestoreConstValue();
        }

        private void restoreValueToolStripButton_Click_1(object sender, EventArgs e)
        {
            RestoreConstValue();
        }

        private void restoreTypeToolStripButton_Click_1(object sender, EventArgs e)
        {
            RestoreConstType();
        }
    }
}
