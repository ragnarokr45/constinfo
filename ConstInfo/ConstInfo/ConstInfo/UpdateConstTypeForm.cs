﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.DataReceiver;
using ConstInfo.Result;
using ConstInfo.Models;
using ConstInfo.Result.RsultForForm.TypesResults;
using ConstInfo.Alerts;

namespace ConstInfo
{
    public partial class UpdateConstTypeForm : Form
    {
        ConstType _constType;
        int _id;
        public UpdateConstTypeForm(int id)
        {
            InitializeComponent();
            _id = id;
            GetData();
        }

        private void GetData()
        {
            _constType = GetConstTypeByIdResult.Execute(_id);
            FillForm();
        }

        private void FillForm()
        {
            if (_constType != null)
            {
                nameTextBox.Text = _constType.ConstName;
            }
        }

        private void UpdateConstType()
        {
            if (PutConstTypeResult.Execute(_constType.Id, nameTextBox.Text))
            {
                this.Close();
            }
        }

        private void updateConstTypeButton_Click(object sender, EventArgs e)
        {
            if (MessageBoxAlerts.SureMessage())
            {
                UpdateConstType();
            }
        }
    }
}
