﻿using ConstInfo.Serializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ConstInfo;
using ConstInfo.Result;
using NLog;

namespace ConstInfo.DataReceiver
{
    abstract class BaseDataHttpReceiver<O, T, U> where O : BaseDataHttpReceiver<O, T, U>
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        protected HttpWebRequest _httpWebRequest;
        protected string _url;
        protected U _sentData;
        protected RestMethods _restMethods;
        public abstract void SetHttpRequestSettings();

        public abstract T QueryToServer();


        private T ExecuteOperation()
        {
            SetHttpRequestSettings();
            return QueryToServer();
        }

        public static T Execute(string url, RestMethods restMethod, params object[] values)
        {
           
            var operation = (O)Activator.CreateInstance(typeof(O), values);
            //logger.Info($"{operation.GetType()} was called");
            operation._url = url;
            operation._restMethods = restMethod;
            return operation.ExecuteOperation();
        }
    }
}
