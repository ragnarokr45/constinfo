﻿using ConstInfo.Models;
using ConstInfo.Serializers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConstInfo.DataReceiver
{
    class LoginHttpRequest : BaseDataHttpReceiver<LoginHttpRequest, bool, string>
    {
        public LoginHttpRequest(string sentData)
        {
            _sentData = sentData;
        }
        public override void SetHttpRequestSettings()
        {
            _httpWebRequest = (HttpWebRequest)WebRequest.Create(_url);
            _httpWebRequest.Method = _restMethods.ToString();
            _httpWebRequest.ContentType = "application/x-www-form-urlencoded";
        }

        public override bool QueryToServer()
        {
            if (_sentData != null)
            {
                using (var streamWriter = new StreamWriter(_httpWebRequest.GetRequestStream(), Encoding.UTF8))
                {
                    streamWriter.Write(_sentData);
                    streamWriter.Flush();
                }
            }

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)_httpWebRequest.GetResponse())
                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    string jsonStr = streamReader.ReadToEnd();
                    Storage.Instance.User = JsonSerialDeserial.Deserialize<User>(jsonStr);
                    Storage.Instance.Token = JsonSerialDeserial.Deserialize<Token>(jsonStr);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
