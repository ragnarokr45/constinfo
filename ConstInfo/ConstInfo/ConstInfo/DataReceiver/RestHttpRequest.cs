﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstInfo.Result;
using System.IO;
using System.Net;
using ConstInfo.Serializers;
using ConstInfo.Alerts.Errors;
using ConstInfo.Alerts;

namespace ConstInfo.DataReceiver
{
    class RestHttpRequest<T, U> : BaseDataHttpReceiver<RestHttpRequest<T, U>, T, U> where T : BaseResult
    {
        public RestHttpRequest()
        {

        }
        public RestHttpRequest(U sentData)
        {
            _sentData = sentData;
        }   
            
        public override void SetHttpRequestSettings()
        {
            _httpWebRequest = (HttpWebRequest)WebRequest.Create(_url);
            _httpWebRequest.Method = _restMethods.ToString();
            _httpWebRequest.ContentType = "application/json; charset=utf-8";
            _httpWebRequest.Accept = "application/json; charset=utf-8";
            _httpWebRequest.Headers["Authorization"] = "Bearer " + Storage.Instance.Token?.AccessToken;
        }

        public override T QueryToServer()
        {
            T result;
            if (_sentData != null)
            {
                string dataStr = _sentData.Serialize<U>();
                using (var streamWriter = new StreamWriter(_httpWebRequest.GetRequestStream(), Encoding.UTF8))
                {
                    streamWriter.Write(dataStr);
                    streamWriter.Flush();
                }
            }
            using (HttpWebResponse response = (HttpWebResponse)_httpWebRequest.GetResponse())
            using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
            {
                result = JsonSerialDeserial.Deserialize<T>(streamReader.ReadToEnd());
            }
            if (!ErrorMassageAlert.Execute(result.ErrorCode, result.ErrorDescription))
            {
                return default(T);
            }
            return result;

        }      
    }
}
