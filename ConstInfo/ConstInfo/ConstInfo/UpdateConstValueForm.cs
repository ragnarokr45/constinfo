﻿using ConstInfo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConstInfo.DataReceiver;
using ConstInfo.Result;
using ConstInfo.Validators;
using ConstInfo.Result.RsultForForm.ValuesResults;
using ConstInfo.Result.RsultForForm.UserResults;
using ConstInfo.Result.RsultForForm.BranchResults;
using ConstInfo.Alerts;

namespace ConstInfo
{
    public partial class UpdateConstValueForm : Form
    {
        ConstValue _constValue;
        List<User> _users;
        List<Branch> _branches;
        int _id;
        public UpdateConstValueForm(int id)
        {
            InitializeComponent();
            _id = id;
            GetConstvalue();
            GetBranches();
            AdminPower();
        }

        private void GetConstvalue()
        {
            _constValue = GetConstValueByIdResult.Execute(_id);
            FillForm();
        }

        private void GetBranches()
        {
            _branches = GetBranchResult.Execute(false);
            if (_branches != null)
            {
                foreach (var item in _branches)
                {
                    ComboboxItem cb = new ComboboxItem();
                    cb.Text = item.BranchName;
                    cb.Value = item.Id;
                    branchComboBox.Items.Add(cb);
                }
                branchComboBox.SelectedIndex = branchComboBox.FindStringExact(_constValue.Branch.BranchName);
            }
        }

        private void AdminPower()
        {
            var checkAdmin = new CheckAdmin();
            if (checkAdmin.Validate())
            {
                _users = GetUserResult.Execute(false);
                if (_users != null)
                {
                    _users = _users.OrderBy(x => x.SecondName).ThenBy(x => x.FirstName).ToList();
                    foreach (var item in _users)
                    {
                        ComboboxItem cb = new ComboboxItem();
                        cb.Text = item.SecondName == "" && item.FirstName == "" ? item.Login : $"{item.SecondName} {item.FirstName}";
                        cb.Value = item.Id;
                        usersComboBox.Items.Add(cb);
                    }
                    string findString = _constValue.User.SecondName == "" && _constValue.User.FirstName == "" ?
                        _constValue.User.Login :
                        $"{_constValue.User.SecondName} {_constValue.User.FirstName}";
                    usersComboBox.SelectedIndex = usersComboBox.FindStringExact(findString);

                }
                usersComboBox.Visible = true;
                userLabel.Visible = true;
            }
        }

        private void FillForm()
        {
            if (_constValue != null)
            {
                nameTextBox.Text = _constValue.Name;
                valueTextBox.Text = _constValue.Value;
                remarkTextBox.Text = _constValue.Remark;
            }
        }

        private void UpdateConstValue()
        {
            User user = _users.Find(x => x.Id == (usersComboBox.SelectedItem as ComboboxItem).Value);
            Branch branch = _branches.Find(x => x.Id == (branchComboBox.SelectedItem as ComboboxItem).Value);
            if (PutConstValueResult.Execute(
                _constValue.ValueId,
                nameTextBox.Text,
                valueTextBox.Text,
                branch,
                remarkTextBox.Text,
                _constValue.Date,
                _constValue.ConstType,
                user
                ))
            {
                this.Close();
            }
        }

        private void updateConstValueButton_Click(object sender, EventArgs e)
        {
            if (MessageBoxAlerts.SureMessage())
            {
                UpdateConstValue();
            }
        }
    }
}
